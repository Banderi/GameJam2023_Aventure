extends Button

export(String) var music = ""
export(String) var cutscene = ""
export(String) var spawner = ""

func _on_Button_pressed():

	if spawner != "":
		Game.call("load_level",name,spawner)
	else:
		Game.call("load_level",name)

func _ready():
	text = name
