extends Control

var skill_name = "" setget set_skill_name
func set_skill_name(skill):
	skill_name = skill
	$CheckBox/Label.text = skill_name

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if skill_name != "":
		var checked = Game.is_skill_unlocked(skill_name)
		if checked == null:
			checked = false
		$CheckBox.pressed = checked

func _on_CheckBox_toggled(button_pressed):
	if skill_name != "":
		Game.unlock_skill(skill_name, button_pressed)
