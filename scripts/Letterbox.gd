extends Control

signal finished

var expansion = 0
var expansion_start = 0
var expansion_goal = 0
var expansion_time = 1.0

var expansion_lerp = false
func expand(goal, time, do_lerp = true):
	Cutscenes.push_gamestate("cutscene_letterboxing")
	t = 0
	expansion_start = expansion
	expansion_goal = goal
	expansion_time = time
	expansion_lerp = do_lerp
	yield(self, "finished")
	Cutscenes.pop_gamestate()

var t = 0
func _process(delta):
	# animation
	var diff = expansion_goal - expansion
	if abs(diff) > 0.01:
		t += delta
		if t >= expansion_time:
			expansion = expansion_goal
			t = 0
			emit_signal("finished")
		else:
			if expansion_lerp:
				expansion = Viewports.clampdamp(expansion, expansion_goal, 0.99999, delta * 5 / expansion_time)
			else:
				expansion += (expansion_goal - expansion_start) * delta / expansion_time
	else:
		expansion = expansion_goal
		t = 0
		emit_signal("finished")

	# net scale
	var window_size = OS.get_window_size().y
	var size = window_size * 0.5 * expansion
	$Top.rect_size.y = size
	$Bottom.rect_size.y = size
