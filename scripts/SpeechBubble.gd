extends Sprite

var target = Vector3()

func _process(delta):
	if is_visible_in_tree():
		set_global_position(Viewports.world3D_to_screen(Game.camera, target))
