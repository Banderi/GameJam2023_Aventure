extends Area

func _physics_process(delta):
	if Game.get_flag("unlocked_high_mountains"):
		return
	for n in get_overlapping_bodies():
		if n == Game.player:
			Game.player.impulse += Vector3(-2,-0.5,-2)
