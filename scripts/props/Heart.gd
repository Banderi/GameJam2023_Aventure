extends RigidBody

export(int) var health = 10

var picked_up = false
func pick_up():
	if picked_up:
		return
	Game.player_restore_hp(health)
	picked_up = true
	queue_free()

var t = 0
func _process(delta):
	t += delta
	if t >= 1:
		$Area.monitoring = true

func _on_Area_body_entered(body):
	if body == Game.player:
		pick_up()

func _ready():
	$Area.monitoring = true
	add_to_group("props")

