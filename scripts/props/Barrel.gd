extends Prop

func hit_floor(prev_velocity):
#	take_hit(abs(prev_velocity.y),Vector3.UP)
	return .hit_floor(prev_velocity)

func take_hit(damage, from, perpetrator):
	Sounds.play_sound("wood_damage.wav",get_3D_center(),1,"SFX")
	for d in (damage / 20):
		Cutscenes.spawn_fx("WoodParticles1",get_3D_center())
	return .take_hit(damage, from, perpetrator)

func die():

	Sounds.play_sound("wood_break.wav",get_3D_center(),1,"SFX")
	Cutscenes.spawn_fx("WoodParticles2",get_3D_center())
	AI.drop_common_loot(get_global_translation())
	AI.drop_common_loot(get_global_translation())
	return .die()
