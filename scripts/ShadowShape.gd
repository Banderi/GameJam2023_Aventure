extends CSGCylinder

func _physics_process(delta):
	var parent = get_parent()
	var parent_translation = parent.get_global_translation()
	var r = Viewports.raycast(parent_translation + Vector3(0,1,0), parent_translation + Vector3(0, -9999, 0), [parent])
	if r != null && r.size() > 0:
		show()
		set_global_translation(r.position + Vector3(0,0.01,0))
	else:
		hide()
