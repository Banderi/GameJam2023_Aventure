extends Node

export(String) var bus = "Master" setget set_bus
export(String) var audio_click = "jump.wav"

var dragging = false

func set_bus(bus_name):
	bus = bus_name
	refresh_label($VolumeSlider.value)
func refresh_label(value):
	$Label.text = str(bus,": ",value*100)

func refresh_from_settings():
	$VolumeSlider.value = Settings.get(str("volume_",bus.to_lower()))
	refresh_label($VolumeSlider.value)

var t = 0.5
func _process(delta):
	if dragging:
		t += delta
		if t >= 0.5:
#			Sounds.play_sound(audio_click, null, 1, bus)
			t -= 0.5
	else:
		t = 0.5

func _on_VolumeSlider_drag_ended(value_changed):
	dragging = false
	var setting_name = str("volume_",bus.to_lower())
	Settings.update(setting_name,Sounds.get_volume(bus))
func _on_VolumeSlider_drag_started():
	dragging = true

func _on_VolumeSlider_value_changed(value):
	Sounds.set_volume(bus,value)
	refresh_label(value)

func _ready():
	add_to_group("volume_sliders")
	if !Game.user_settings_loaded:
		yield(Game,"settings_loaded")
	refresh_from_settings()


func _on_Control_gui_input(event):
	if Input.is_action_just_pressed("reset_setting_to_default"):
		var setting_name = str("volume_",bus.to_lower())
		var default_value = Settings.get_default(setting_name)
		$VolumeSlider.value = default_value
		_on_VolumeSlider_drag_ended(default_value)
