extends Area

export(String) var cutscene_call_on_player_walkin = ""

func enable():
	monitoring = true
func disable():
	monitoring = false

func _on_AreaTrigger_body_entered(body):
	if monitoring && body == Game.player:
		if cutscene_call_on_player_walkin != "":
			Cutscenes.call(cutscene_call_on_player_walkin)
#			yield(Cutscenes.call(cutscene_call_on_player_walkin),"completed")
