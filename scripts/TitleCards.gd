extends Control

var expanding = 0

func start(title):
	$Label.text = title
	expanding = 1
	t = 0

var t = 0
func _process(delta):
	t += delta * expanding
	if expanding == 1:
		if t >= 1:
			expanding = 0
			$Timer.start(2)

	modulate.a = clamp(t, 0, 1)


func _on_Timer_timeout():
	expanding = -1
