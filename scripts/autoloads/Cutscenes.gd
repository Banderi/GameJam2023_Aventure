extends Node

func _ready():
	self.set_pause_mode(2) # Set pause mode to Process
	set_process(true)

func _empty_yield():
	yield(Engine.get_main_loop(), "idle_frame")

var LETTERBOX = null
func letterbox(goal, time, do_lerp = true):
	yield(LETTERBOX.expand(goal, time, do_lerp),"completed")
func is_letterboxing():
	return LETTERBOX.expansion != 0

func fade_out(seconds):
	pass
func fade_in(seconds):
	pass

func spawn_fx_under_node(parent, fx, position, params = {}):
	var path = str("res://scenes/FX/",fx,".tscn")
	var scene = load(path)
	var node = scene.instance()
	for p in params:
		if params[p] != null && p in node:
			node.set(p, params[p])
	if position is Vector3:
		Game.LEVEL.add_child(node) if parent == null else parent.add_child(node)
		node.set_global_translation(position)
	else:
		Game.UI.add_child(node) if parent == null else parent.add_child(node)
		node.set_global_position(position)
	return node
func spawn_fx(fx, position, params = {}):
	return spawn_fx_under_node(null, fx, position, params)

func spawn_stomp(position, radius, damage, caster = null, ignore_friends = false):
	spawn_fx("StompFX", position, {"radius":radius,"damage":damage,"caster":caster,"ignore_friends":ignore_friends})

func spawn_text(position, text, color, speed = 1.5, jumpiness = 5, gravity = -0.25):
	return spawn_fx("TextFX", position, {"text":str(text),"modulate":color,"speed":speed,"gravity":gravity,"jumpiness":jumpiness})
func spawn_projectile(projectile, position, caster, target, damage = null, speed = null, callback = null, callback_params = []):
	var node = spawn_fx(projectile, position,{
		"target":target,
		"caster":caster,
		"damage":damage,
		"speed":speed,
		"callback":callback,
		"callback_params":callback_params
	})
	if node.spawn_sound != "":
		Sounds.play_sound(node.spawn_sound, position, node.spawn_sound_volume, "SFX")
	return node

func wait(time):
	var timer = Timer.new()
	timer.set_wait_time(time)
	timer.set_one_shot(true)
	self.add_child(timer)
	timer.start()
	yield(timer, "timeout")
	timer.queue_free()

var DELICATE = null
var queued_deferred = false
var skip_deferred = false
func delicate_deferred(visible): # queue up call!
	queued_deferred = true
	call_deferred("delicate", visible, true)
func delicate(visible, deferred = false):
	if deferred:
		queued_deferred = false
		# a separate non-queued call was made before this fired, so we drop this
		if skip_deferred:
			skip_deferred = false
			return
	# if another call was queued, skip
	elif queued_deferred:
		skip_deferred = true
	DELICATE.visible = visible

var prev_gamestate = null
func push_gamestate(state):
	prev_gamestate = Game.game_state
	Game.set_game_state(state)
func pop_gamestate():
	Game.set_game_state(prev_gamestate)

var camera_position = null
var camera_target = null
var camera_speed = null
var camera_flyby = false
func camera_goto_euler(position, euler, flyby, speed):
	camera_position = position
	var direction = Vector3(0,0,-1)\
	.rotated(Vector3(1,0,0), deg2rad(euler.x))\
	.rotated(Vector3(0,1,0), deg2rad(euler.y))
	camera_target = position + direction
	camera_flyby = flyby
	camera_speed = speed
func camera_goto(position, target, flyby, speed):
	camera_position = position
	camera_target = target
	camera_flyby = flyby
	camera_speed = speed
func camera_follow(target):
	camera_target = target

func actor_goto(actor, dirty_navpath_index, speed):
	if Game.is_valid_objref(actor):
		var dirty_navpath = null
		for node in get_tree().get_nodes_in_group("dirty_navpaths"):
			if node.name == str("Navpath",dirty_navpath_index):
				dirty_navpath = node
				break
		if dirty_navpath == null:
			return yield(_empty_yield(),"completed")
		actor.set_navpath(dirty_navpath, speed, true, true)
		yield(actor, "navpath_completed")
	yield(_empty_yield(),"completed")
func actor_lookat(actor, position):
	if Game.is_valid_objref(actor):
		if position is Actor3D:
			position = position.get_global_translation()
		actor.lookat.set_global_translation(position)
func get_node(node_name):
	return Game.LEVEL.get_node(node_name)

func change_mesh_texture(node, surface, new_texture):
	for n in node.get_children():
		if n is MeshInstance:
			var material = n.mesh.surface_get_material(surface)
			material.albedo_texture = load(str("res://assets/3D/",new_texture))
			return

var PORTRAITS = null
func portrait(character):
	if character == null:
		PORTRAITS.hide()
	else:
		PORTRAITS.show()
		for n in PORTRAITS.get_node("Characters").get_children():
			if n.name == character:
				n.show()
			else:
				n.hide()

var TITLE_CARD = null
func title_card(title):
	TITLE_CARD.start(title)

var OBJECTIVE_LABEL = null
var objective_label_text = ""
var objective_label_t = 0
func objective_message(text):
	OBJECTIVE_LABEL.text = ""
	if text == null:
		text = ""
	objective_label_t = 0
	objective_label_text = text

signal dialogue_terminated

var DIALOGUE_BOX = null
var DIALOGUE_BUTTON = null
var DIALOGUE_BBCODE = null
var DIALOGUE_BUBBLE = null
var DIALOGUE_TIMER = null

var dialogue_id = null
var dialogue_stack_index = 0
var dialogue_plain_text_buffer = ""
func dialogue_start(id, end_gamestate = null): # ALWAYS assume the params aren't cleared from last time!
	if end_gamestate != null:
		push_gamestate(end_gamestate)
	push_gamestate("dialogue")
	dialogue_id = id
	dialogue_stack_index = -1
	text_inputs_force_unpress = true
	dialogue_next()
	yield(self, "dialogue_terminated")
func dialogue_inserts_safe_newlines(): # this assumes dialogue_plain_text_buffer is filled and up to date!
	# dirty fix to make sure that the character-by-character typing aligns
	# correctly with the bbcode text.
	# if it doesn't, it will appear incorrectly colored (red)!
	var index = 0
	while dialogue_plain_text_buffer.length() - index >= 38:
		var n = dialogue_plain_text_buffer.rfind("\n",index + 37)
		if n >= index:
			index = n + 1
			continue
		var i = dialogue_plain_text_buffer.rfind(" ",index + 37)
		dialogue_plain_text_buffer[i] = "\n"
		index = i + 1
func dialogue_next():
	if dialogue_stack_index >= DIALOGUES[dialogue_id].size() - 1:
		dialogue_end()
	else:
		dialogue_stack_index += 1

		# fetch and apply bbcode
		var dialogue_data = DIALOGUES[dialogue_id][dialogue_stack_index]
		var bbcode_text = ""
		for line in dialogue_data.bbcode:
			bbcode_text += str(line,"\n")
		DIALOGUE_BBCODE.clear()
		DIALOGUE_BBCODE.append_bbcode(bbcode_text)

		# save the plain text and reset the text cropper's head
		dialogue_plain_text_buffer = DIALOGUE_BBCODE.text
		dialogue_inserts_safe_newlines()
		DIALOGUE_BOX.clear()
		dialogue_textcrop_head = 0
		t = 0
		text_speed = 1.0
		text_speed = float(dialogue_data.get("speed", 20.0))

		# change font default color based on special game characters
		DIALOGUE_BBCODE.set("custom_colors/default_color", Color(1,1,1))
		if dialogue_data.name == "Asterius":
			DIALOGUE_BBCODE.set("custom_colors/default_color", Color("#B8860B"))

		# speech bubble sprite
		for actor in get_tree().get_nodes_in_group("actors"):
			var check_name = dialogue_data.name.to_lower()
			var actor_name = actor.name.to_lower()
			if check_name in actor_name:
				DIALOGUE_BUBBLE.target = actor.get_3D_height() + Vector3(0,0.5,0)

		# show the box last, so it's all ready for displaying!
		DIALOGUE_BUTTON.hide()
		DIALOGUE_BOX.show()
func dialogue_end(): # this hides the box but doesn't clear the previous params.
	DIALOGUE_BOX.hide()
	pop_gamestate()
	emit_signal("dialogue_terminated")

var dialogue_textcrop_head = 0
var t = 0
var text_speed = 1.0
var text_inputs_pressed_scancode = null
var text_inputs_force_unpress = false
func dialogue_delta_step(delta):
	if Game.game_state != "dialogue":
		return
	t += delta
	var d_char = 1.0 / text_speed
	if text_inputs_pressed_scancode != null && !text_inputs_force_unpress:
		d_char = 0.001
	if DIALOGUE_BOX.text.length() >= dialogue_plain_text_buffer.length():
		# text is done!
		DIALOGUE_BUTTON.rect_size.y = 50 + 10*sin(2*t)
		if text_inputs_pressed_scancode != null && !text_inputs_force_unpress:
			text_inputs_pressed_scancode = null
			dialogue_next()
	else: # print characters one by one
		while t >= d_char:
			t -= d_char
			if DIALOGUE_BOX.text.length() < dialogue_plain_text_buffer.length():
				DIALOGUE_BOX.text += dialogue_plain_text_buffer[dialogue_textcrop_head]
				dialogue_textcrop_head += 1

				# reset input listening when we reach the end of the text
				if DIALOGUE_BOX.text.length() >= dialogue_plain_text_buffer.length():
					text_inputs_force_unpress = true
					DIALOGUE_BUTTON.show()
func objective_label_step(delta):
	objective_label_t -= delta
	if OBJECTIVE_LABEL.text.length() < objective_label_text.length():
		OBJECTIVE_LABEL.modulate.a = 1.0
		if objective_label_t <= 0:
			OBJECTIVE_LABEL.text += objective_label_text[OBJECTIVE_LABEL.text.length()]
			objective_label_t += 0.025
	else:
		if objective_label_t > -999:
			OBJECTIVE_LABEL.modulate.a = clamp(0.5 * objective_label_t + 3.0,0,1)
func _process(delta):
	dialogue_delta_step(delta)
	objective_label_step(delta)

func _input(event):
	if !("pressed" in event):
		return
	var _dirty_scancode = Keybinds.get_dirty_scancode(event)
	if _dirty_scancode == null:
		return
	if Keybinds.is_dirty_scancode_pressed(_dirty_scancode):
		if text_inputs_pressed_scancode == null || !text_inputs_force_unpress:
			text_inputs_pressed_scancode = _dirty_scancode
	else:
		text_inputs_pressed_scancode = null
		text_inputs_force_unpress = null

### CUTSCENES
func cutscene_tutorial_messages():
	Cutscenes.objective_message(str(
		Keybinds.get_action_key_text("move_up"),
		Keybinds.get_action_key_text("move_left"),
		Keybinds.get_action_key_text("move_down"),
		Keybinds.get_action_key_text("move_right"),
		": move"))
	yield(wait(8),"completed")
	if Game.get_flag("senigallius_3_arena"): return
	Cutscenes.objective_message(str(
		Keybinds.get_action_key_text("attack")," / left mouse",
		": attack"))
	yield(wait(8),"completed")
	if Game.get_flag("senigallius_3_arena"): return
	Cutscenes.objective_message(str(
		"Right mouse: rotate camera"))
	yield(wait(8),"completed")
	if Game.get_flag("senigallius_3_arena"): return
	Cutscenes.objective_message(str(
		Keybinds.get_action_key_text("lockon"),
		": lock on target"))
	yield(wait(8),"completed")
	if Game.get_flag("senigallius_3_arena"): return
	Cutscenes.objective_message(str(
		Keybinds.get_action_key_text("sprint"),
		": sprint"))
	yield(wait(8),"completed")
	if Game.get_flag("senigallius_3_arena"): return
	Cutscenes.objective_message(str(
		Keybinds.get_action_key_text("use_skill"),
		": use skill"))

func cutscene_senigallius1(): # ...
	Game.set_game_state("cutscene")
	yield(letterbox(0.25, 1.5),"completed")
	yield(wait(4),"completed")

	# Senigallius intro
	portrait("Senigallius")
	yield(dialogue_start("senigallius1"),"completed")
	portrait(null)
	camera_goto_euler(Vector3(1.5, 2.6, 58.4), Vector3(-25,-14,0), false, 1)
	yield(dialogue_start("senigallius2"),"completed")

	# end, free camera, ingame
	yield(letterbox(1, 1),"completed")
	camera_goto(null, null, false, 1)
	yield(wait(0.5),"completed")
	yield(letterbox(0, 1),"completed")
	Game.set_flag("senigallius_1",true)
	Game.set_game_state("ingame")
	cutscene_tutorial_messages()
func cutscene_senigallius_cantleave(): # I can't leve now!
	Game.set_game_state("cutscene")
	yield(letterbox(0.25, 0.5),"completed")

	yield(dialogue_start("senigallius_cantleave"),"completed")
	yield(actor_goto(Game.player,1,5),"completed")

	yield(letterbox(0, 0.5),"completed")
	Game.set_game_state("ingame")
func cutscene_senigallius2(): # ugh, no end to these creatures!
	# it says 2 but actually the dialogue is 3 (it's correct)
	if Game.get_flag("senigallius_2"):
		return
	Game.set_game_state("cutscene")
	yield(letterbox(0.25, 0.5),"completed")

	# ugh, no end to these creatures!
	yield(dialogue_start("senigallius3"),"completed")

	yield(letterbox(0, 0.5),"completed")
	Game.set_flag("senigallius_2",true)
	Game.set_game_state("ingame")
func cutscene_senigallius3_arena(): # uh oh, a miniboss!
	if !Game.get_flag("senigallius_3_arena"):
		Game.set_game_state("cutscene")
		Game.lock_saving(true,"Can not save during a boss fight!")
		yield(letterbox(0.25, 0.5),"completed")
		yield(wait(1),"completed")

		camera_goto(Vector3(18,1,-30),Vector3(18,0,-33),false,1)
		yield(wait(1),"completed")

		get_node("ArenaWalls/CollisionShape").disabled = false
		get_node("ArenaWalls2/CollisionShape").disabled = false

		# spider appears
		get_node("Spooder").show()
		get_node("Spooder").movement_type = 3
		get_node("Spooder").mass = 1

		# fiend!!!
		yield(wait(1),"completed")
		yield(dialogue_start("senigallius_miniboss"),"completed")
		yield(wait(0.5),"completed")

		get_node("AreaTrigger3").disable()
		get_node("Spooder").connect("actor_died",self,"cutscene_senigallius_end")
		camera_goto(null,null,null,null)
		yield(letterbox(0, 0.5),"completed")
		Game.set_game_state("ingame")
		Game.set_flag("senigallius_3_arena",true)
func cutscene_senigallius_end():
	Game.set_game_state("cutscene")
	yield(letterbox(0.25, 0.5),"completed")
	# defeated the miniboss

	# pant...
	yield(dialogue_start("senigallius_end1"),"completed")

	# "now back to searching for khelys"
	camera_goto(Vector3(19,0,-36),Vector3(17,-1,-42),false,1)
	yield(actor_goto(Game.player,2,5),"completed")

	# activate asterius
	var asterius = get_node("Asterius")
	asterius.movement_type = 3
	asterius.mass = 1
	asterius.move_and_collide(Vector3(0,-99999,0))
	yield(dialogue_start("senigallius_end1b"),"completed")

	# show asterius
	asterius.show()
	Game.player = asterius
	yield(actor_goto(asterius,3,2),"completed")

	# splat
	yield(wait(2),"completed")
	asterius.attack_cooldown = 0.25
	asterius.attack_used = 0
	Sounds.play_sound("sock.wav",asterius.get_3D_center(),2,"SFX")
	get_node("Senigallius").take_hit(9999,Vector3(0,0,-1),asterius)
	yield(wait(2),"completed")

	# asterius enters the scene
	yield(dialogue_start("senigallius_end2"),"completed")

	# look at skeleton
	actor_lookat(asterius,Vector3(14.31,-1.01,-33.72))
	camera_goto(Vector3(17.4,0.3,-34),Vector3(14.31,-1.01,-33.72),true,0.5)
	yield(dialogue_start("senigallius_end3"),"completed")

	# (rant) I swear...
	camera_goto(null,null,null,null)
	yield(wait(0.5),"completed")
	yield(dialogue_start("senigallius_end4"),"completed")

	# hmm... that's it!!
	yield(dialogue_start("senigallius_end5"),"completed")

	# end
	actor_goto(asterius,4,8)
	yield(wait(0.5),"completed")
	yield(letterbox(1, 0.25),"completed") # fade-out, load next level...
	yield(wait(2),"completed")
	Game.load_level("01_library")

func cutscene_library():
	Game.set_game_state("cutscene")
	yield(letterbox(1, 0),"completed")
#	camera_goto_euler(Vector3(2,3.8,3.5), Vector3(-40,-10,0), false, -1)
	camera_goto(Vector3(2,3.8,3.5), Vector3(3,0,0), false, -1)
	yield(letterbox(0.5, 0.5),"completed")
	yield(wait(1),"completed")

	# turn camera towards door slowly
	camera_goto(Vector3(0,3.8,3.5), Game.player, true, 0.1)
	yield(wait(6),"completed")

	# bat goes "?"
	var bat = get_node("Bat")
	spawn_text(bat.get_global_translation(),"?",Color(0.75,0.75,0.75),0.25,1,-0.01)
	actor_lookat(bat,Game.player)

	# Asterius breaks into room
	yield(wait(2),"completed")
	get_node("Particles").emitting = true
	get_node("Particles2").emitting = true
	Sounds.play_sound("wood_break.wav",null,0.25,"SFX")
	Sounds.play_sound("wood_damage.wav",null,0.25,"SFX")
	get_node("Asterius").show()
	change_mesh_texture(get_node("01_library"), 0, "levels/library2.png")
	spawn_text(bat.get_global_translation(),"!",Color(1,1,0),0.25,2.5,-0.105)
	bat.aiming_at_target = Game.player
	spawn_stomp(Vector3(-3,0,0), null, -1)

	# dialogue 1
	yield(wait(1),"completed")
	yield(dialogue_start("library1"),"completed")

	# follow player, walk over to librarian
	camera_goto(Vector3(0,3.8,3.5), Game.player, false, 1)
	yield(actor_goto(Game.player,1,2.5),"completed")
	yield(dialogue_start("library2"),"completed")

	# walk over to bookshelf
	yield(actor_goto(Game.player,2,2.5),"completed")
	yield(dialogue_start("library3"),"completed")

	# walk out of library
	yield(actor_goto(Game.player,3,5),"completed")
	get_node("Asterius").hide()
	yield(dialogue_start("library4"),"completed")

	# end
	yield(letterbox(1, 0.25),"completed") # fade-out, load next level...
	Game.load_level("02_mountain","SpawnerStart")

func cutscene_jotunheim_arrive():
	Game.player_full_hp()
	Game.set_game_state("cutscene")
	yield(letterbox(1, 0),"completed")
	yield(letterbox(0.5, 0.5),"completed")

	yield(actor_goto(Game.player,1,2.5),"completed")

	# Asterius reaches the base of the mountains
	yield(dialogue_start("jotunheim1"),"completed")

	yield(letterbox(0, 0.5),"completed")
	Game.set_flag("senigallius_finale",true)
	Game.set_game_state("ingame")
	get_node("AreaTrigger2").enable()
func cutscene_jotunheim_cantleave(): # I can't leve now!
	Game.set_game_state("cutscene")
	yield(letterbox(0.25, 0.5),"completed")

	yield(dialogue_start("jotunheim_cantleave"),"completed")
	yield(actor_goto(Game.player,1,5),"completed")

	yield(letterbox(0, 0.5),"completed")
	Game.set_game_state("ingame")
func cutscene_jotunheim_demowall():
	Game.set_game_state("cutscene")
	yield(letterbox(0.25, 0.5),"completed")

	yield(dialogue_start("demowall"),"completed")
	yield(actor_goto(Game.player,2,5),"completed")

	yield(letterbox(0, 0.5),"completed")
	Game.set_game_state("ingame")

func cutscene_alchemists():
	Game.set_game_state("cutscene")
	yield(letterbox(1, 0),"completed")
	actor_lookat(Game.player,get_node("NPC_Pauli"))
	camera_goto(Vector3(-4.7,3.5,3.8), Vector3(0,0,0.4), false, 1)
	yield(letterbox(0.5, 0.5),"completed")

	yield(wait(0.5),"completed")
	actor_lookat(get_node("NPC_Pauli"),Game.player)
	yield(dialogue_start("alchemists1a"),"completed") # uhh

	actor_lookat(get_node("NPC_Wolfgang"),Game.player)
	actor_lookat(Game.player,get_node("NPC_Wolfgang"))
	yield(dialogue_start("alchemists1b"),"completed") # ahhh

	yield(dialogue_start("alchemists1c"),"completed") # proposal

	Game.player_increase_max_hp_by(50)

	camera_goto(null,null,null,null)

	yield(letterbox(0, 0.5),"completed")
	Game.set_flag("npc_alchemists_met",true)
	Game.set_game_state("ingame")
func cutscene_wolf():
	pass
func cutscene_mountain_pass():
	if Game.get_flag("unlocked_high_mountains"):
		Game.load_level("03_highmountains","SpawnerStart")
	else:
		Game.set_game_state("cutscene")
		yield(letterbox(0.5, 0.5),"completed")
		yield(wait(0.5),"completed")

		if Game.get_flag("npc_alchemists_received_prize"):
			# magic spell
			var spell_node = get_node("AntiBlizzardSpell")
			camera_goto(Vector3(33,46,0), spell_node.get_global_translation(),false,1)
			yield(wait(2),"completed")
			spell_node.emitting = true
			var sound = Sounds.play_sound("illuminati.mp3",null,1,"SFX")
			yield(wait(2),"completed")
			sound.queue_free()
			camera_goto(null,null,null,null)
			spell_node.emitting = false
			Game.set_flag("unlocked_high_mountains",true)
		else:
			# guh!!
			Game.player.movement_type = 3
			yield(dialogue_start("mountains_impassable"),"completed")

		yield(letterbox(0, 0.5),"completed")
		Game.set_game_state("ingame")
func cutscene_mountain_goback():
	Game.set_flag("unlocked_high_mountains",true)
	Game.set_flag("mountains_impassable",true)
	Game.set_flag("npc_alchemists_received_prize",true)
	Game.load_level("02_mountain","SpawnerMuspelheim")

func cutscene_muspelheim_arrive():
	Game.set_game_state("cutscene")
	yield(letterbox(1, 0),"completed")
	yield(letterbox(0.5, 0.5),"completed")

	yield(wait(2),"completed")
	yield(dialogue_start("muspelheim_arrive"),"completed") # coming for you!!

	yield(letterbox(0, 0.5),"completed")
	Game.set_flag("muspelheim_arrived",true)
	Game.set_game_state("ingame")
func cutscene_wolves_ambush():
	Game.set_game_state("cutscene")
	yield(letterbox(0.25, 0.5),"completed")

	yield(wait(2),"completed")
	camera_goto(Vector3(24,38,15),Game.player,false,1)
	actor_lookat(Game.player, Vector3(22.4,31,10.7)) # look behind
	Sounds.play_sound("wolf_howl2.ogg",null,4,"SFX") # howls uh oh

	yield(wait(1),"completed")
	Sounds.play_sound("gasp.wav",null,1,"SFX") # !
	Cutscenes.spawn_text(Game.player.get_3D_height(),"!",Color(1,1,0),0.25,2.5,-0.105)

	for wolf in [
#		get_node("Wolf"),
#		get_node("Wolf2"),
		get_node("Wolf3"),
		get_node("Wolf4"),
		get_node("Wolf5"),
	]: # activate wolves
		actor_lookat(wolf, Game.player)
		wolf.show()
		AI.force_aggro(wolf, Game.player)
		spawn_fx("SmokePuffFX", wolf.get_3D_center())

	yield(wait(1),"completed")
	get_node("AreaTriggerWolves").disable() # deactivate wolves

	camera_goto(null,null,null,null)
	yield(letterbox(0, 0.5),"completed")
	Game.set_flag("senigallius_finale",true)
	Game.set_game_state("ingame")
func cutscene_giants_bossroom(): # this is auto-disabled, there's no "boss activated" flag
	Sounds.play_music("")
	Game.set_game_state("cutscene")
	Game.lock_saving(true,"Can not save during a boss fight!")
	yield(letterbox(0.5, 0.5),"completed")

	yield(wait(1),"completed")
	camera_goto(Vector3(32,48,-55.5),Vector3(27.6,47.5,-47.4),false,1) # look at the entrance

	yield(wait(0.5),"completed")
	get_node("ArenaWalls/CollisionShape").disabled = false # block off way back
	get_node("ArenaWalls2/CollisionShape").disabled = false # block off way back
	get_node("ArenaMagicWall").emitting = true
	get_node("ArenaMagicWall2").emitting = true
	Sounds.play_sound("slavine.wav",null,1,"SFX")

	yield(wait(0.5),"completed")
	yield(dialogue_start("giants_boss_1"),"completed") # ...

	yield(wait(1),"completed")
	camera_goto(Vector3(29,49,-58),Vector3(23.4,44.5,-93.7),false,1) # look at ice lake center

	for giant in [get_node("Giant1"),get_node("Giant2")]: # activate giants
		giant.show()
		giant.movement_type = 3
		giant.mass = 1
		giant.connect("actor_died",self,"cutscene_giants_boss_defeated")

	yield(wait(2),"completed")
	title_card("Twin Giants") # boss title card
	Sounds.play_sound("wolf_kill1.wav",null,1,"SFX") # ROAR!!!
	Sounds.play_music("boss_twin_giants.ogg")

	yield(wait(4),"completed")
	yield(dialogue_start("giants_boss_2"),"completed") # bring it on!!!

	# end cutscene
	get_node("AreaTriggerBoss").disable()
	camera_goto(null,null,null,null)

	yield(letterbox(0, 0.5),"completed")
	Game.set_game_state("ingame")

func cutscene_giants_boss_defeated():
	var is_dead1 = Game.is_actor_dead(get_node("Giant1"))
	var is_dead2 = Game.is_actor_dead(get_node("Giant2"))
	print(str("1:",is_dead1," 2:",is_dead2))
	if is_dead1 && is_dead2:
		Sounds.play_music("")
#		Game.set_game_state("cutscene")
		Game.lock_saving(false)
		Game.player.movement_type = 3
		yield(letterbox(0.5, 0.5),"completed")
		Game.player.movement_type = 3

		get_node("ArenaWalls/CollisionShape").disabled = true # remove magical barriers
		get_node("ArenaWalls2/CollisionShape").disabled = true
		get_node("ArenaMagicWall").emitting = false
		get_node("ArenaMagicWall2").emitting = false

		yield(wait(2),"completed")
		yield(dialogue_start("giants_boss_3"),"completed") # and stay dead!!

		get_node("AreaTriggerOdin").enable()
		Game.set_flag("BOSS_TWIN_GIANTS",true)

		yield(letterbox(0, 0.5),"completed")
		Game.set_game_state("ingame")

func cutscene_odin_1():
	Game.set_game_state("cutscene")
	yield(letterbox(0.5, 0.5),"completed")

	get_node("AreaTriggerOdin").disable() # disable trigger

	yield(wait(1),"completed")
	camera_goto(Vector3(39,48.6,-76), Vector3(41,50,-80), false, 1) # look at odin

	yield(wait(1),"completed") # egad, it's odin!!
	var odin = get_node("NPC_Odin")
	odin.show()
	odin.fade_in(1)

	yield(wait(1),"completed")
	yield(dialogue_start("odin1"),"completed") # dialogue with odin

	odin.fade_out(1) # odin goes bye
	yield(wait(1),"completed")
	odin.queue_free()
	yield(wait(0.5),"completed")

	camera_goto(null,null,null,null)
	Game.set_flag("npc_odin1",true)
	yield(letterbox(0, 0.5),"completed")
	Game.set_game_state("ingame")

### TEMP
var DIALOGUES = { # the dialogue box can support at most 38 characters per line (monospaced)!
	"test": [
		{"name": "A Random Dude",
		"bbcode": [	"Hey, you!",
					"You're finally [color=yellow]awake[/color].",
					"Those [color=red]Imperial[/color] scum have got at you finally, huh?",
					"I tell ya... One of these days, I..." ]},
		{"name": "Asterius","speed":5,
		"bbcode": [	"????" ]},
		{"name": "Asterius",
		"bbcode": [	"What the [color=#ff00ff]@#?![/color] are you talking about old man??" ]},
	],
	"demowall": [
		{"name": "Asterius",
		"bbcode": [	"Nothing of interest up ahead.." ]},
	],


	"senigallius1": [
		{"name": "Senigallius",
		"bbcode": [	"..." ]},
		{"name": "Senigallius",
		"bbcode": [	"This place gives me the creeps..." ]},
		{"name": "Senigallius",
		"bbcode": [	"I must be brave though.              ",
					"[color=#FF00FF]Khelys[/color] couldn't have gone far." ]},
	],
	"senigallius2": [
		{"name": "Senigallius",
		"bbcode": [	"If I get out of this alive... I swear I'm moving to Knossos." ]},
	],
	"senigallius3": [
		{"name": "Senigallius",
		"bbcode": [	"Guh! These nasty creatures are everywhere." ]},
		{"name": "Senigallius",
		"bbcode": [	"No wonder that [color=#FF0000]monster[/color] was imprisoned in here." ]},
		{"name": "Senigallius",
		"bbcode": [	"I genuinely hope Khelys is ok..." ]},
	],
	"senigallius_cantleave": [
		{"name": "Senigallius",
		"bbcode": [	"I can't go back now. I must find [color=#FF00FF]Khelys[/color]!" ]},
	],

	"senigallius_miniboss": [
		{"name": "Senigallius",
		"bbcode": [	"Unholy fiend!!" ]},
	],

	"senigallius_end1": [
		{"name": "Senigallius",
		"bbcode": [	"*pant*....             \n*pant*.....               " ]},
		{"name": "Senigallius",
		"bbcode": [	"That.. was the last of them, I hope.." ]},
	],
	"senigallius_end1b": [
		{"name": "Senigallius",
		"bbcode": [	"And now, back to searching for\nKhelys!" ]},
	],
	"senigallius_end2": [
		{"name": "Asterius",
		"bbcode": [	"Ugh..            ",
					"These nasty creatures are EVERYWHERE.." ]},
		{"name": "Asterius",
		"bbcode": [	"Will it ever end?!" ]},
	],
	"senigallius_end3": [
		{"name": "Asterius",
		"bbcode": [	"They're not even that tasty...",
					"His [color=#FF00FF]friend[/color] was way too malnourished!" ]},
	],
	"senigallius_end4": [
		{"name": "Asterius",
		"bbcode": [	"I swear to Eileithyia, I can't stand it here anymore!!" ]},
		{"name": "Asterius",
		"bbcode": [	"I'm tired to heck and back of being",
					"stuck here, playing the part of the",
					"villain only for some wannabee \"hero\"",
					"to come along and \"defeat me\"!" ]},
		{"name": "Asterius",
		"bbcode": [	"Everybody thinks they can get an easy life of glory today, they just have to ask a local \"king in need\" for some trials or quests and BAM" ]},
		{"name": "Asterius",
		"bbcode": [	"BAH!!!",
					"I alone am worth FIFTY of them!",
					"If I had my go, I would become, like,",
					"the greatest hero that ever lived!" ]},
	],
	"senigallius_end5": [
		{"name": "Asterius",
		"bbcode": [	"...Hmm..." ]},
		{"name": "Asterius",
		"bbcode": [	"The [color=#FF00FF]greatest hero...[/color]" ]},
		{"name": "Asterius",
		"bbcode": [	"You know WHAT? I've GOT IT." ]},
		{"name": "Asterius",
		"bbcode": [	"I'm tired of being the villain!",
					"I'LL show them what how it's done!" ]},
		{"name": "Asterius",
		"bbcode": [	"I'm gonna find myself some stupid king with some stupid quests and *I'LL* get the bitches!!" ]},
		{"name": "Asterius",
		"bbcode": [	"WATCH OUT, WORLD!",
					"Here comes [color=#FF00FF]Asterius, the GREATEST HERO of all time!![/color]" ]},
	],
				##	"......................................" 	  <--- what 38 characters look like

	"library1": [
		{"name": "Asterius",
		"bbcode": [	"Hmm..." ]},
		{"name": "Asterius",
		"bbcode": [	"This looks like a good place to start." ]},
		{"name": "Asterius",
		"bbcode": [	"You there, human!" ]},
	],

	"library2": [
		{"name": "Asterius",
		"bbcode": [	#"You there, human.",
					"Tell me what you know about heroics." ]},
		{"name": "Librarian",
		"bbcode": [	"Certainly, sir.\nDo you have a library card?" ]},
		{"name": "Asterius",
		"bbcode": [	"No, but I have a giant appetite." ]},
		{"name": "Librarian",
		"bbcode": [	"Heroics... let's see..." ]},
		{"name": "Librarian",
		"bbcode": [	"A very popular hero around these parts is [color=#FF4500]Hercules[/color].",
					"All the kids love him!" ]},
		{"name": "Asterius",
		"bbcode": [	"Hercules, hm... The name sounds familiar..." ]},
		{"name": "Librarian",
		"bbcode": [	"Would you like to me to show you his villa?" ]},
#		{"name": "Asterius",
#		"bbcode": [	"No. I'm looking for unclaimed [color=#FF00FF]HEROIC QUESTS[/color]." ]},
		{"name": "Asterius",
		"bbcode": [	"No!! I'm looking for [color=#FF00FF]GREAT KINGS[/color]!" ]},
#		{"name": "Librarian",
#		"bbcode": [	"Hmm...\nThere really isn't a lot of those around anymore, sadly." ]},
#		{"name": "Asterius",
#		"bbcode": [	"There's got to be some more." ]},
		{"name": "Librarian",
		"bbcode": [	"Hmm...\nThere really isn't a lot of those around anymore, sadly." ]},
		{"name": "Librarian",
		"bbcode": [	"Maybe you can search in the Mythos and Legends section.",
					"Room 1, aisle 1, row 1, second to last shelf." ]},
		{"name": "Asterius",
		"bbcode": [	"There's only one aisle.." ]},
		{"name": "Librarian",
		"bbcode": [	"Yes, that is correct." ]},
	],

	"library3": [
		{"name": "Asterius",
		"bbcode": [	"[color=#FF00FF]Kings...[/color]" ]},
		{"name": "Asterius",
		"bbcode": [	"Odoacre...                     ",
					"Onesiphorus...                 ",
					"Oedipus...                     " ]},
		{"name": "Asterius",
		"bbcode": [	"...ew." ]},
		{"name": "Asterius",
		"bbcode": [	"[color=#FF00FF]Odin...[/color]" ]},
		{"name": "Asterius",
		"bbcode": [	"\"The almighty [color=#FF00FF]king of Valhalla[/color],",
					"patriarch of all the norse gods,",
					"fiercest and cruelest, always presiding over the [color=#FF00FF]greatest warriors of the world[/color]\"" ]},
		{"name": "Asterius",
		"bbcode": [	"Perfect!! Exactly what I need!" ]},
#		{"name": "Asterius",
#		"bbcode": [	"HUMAN!!" ]},
#		{"name": "Librarian",
#		"bbcode": [	"I'm literally five meters away, sir. There's no need to yell." ]},
		{"name": "Asterius",
		"bbcode": [	"I'm gonna look for this Odin, he sounds in need of a strong champion." ]},
	],
	"library4": [
		{"name": "Librarian",
		"bbcode": [	"Uh, sir? That book is property of the libr-" ]},
		{"name": "Librarian",
		"bbcode": [	"...aaand he's gone.\nAlways a month from retirement, of course." ]},
	],

	################### JOTUNHEIM

	"jotunheim1": [
		{"name": "Asterius",
		"bbcode": [	"Why must all kings live so high up??" ]},
		{"name": "Asterius",
		"bbcode": [	"This looks like the right place..." ]},
	],
	"jotunheim_cantleave": [
		{"name": "Asterius",
		"bbcode": [	"I've come too far to go back now!" ]},
		{"name": "Asterius",
		"bbcode": [	"What am I, a human or a minotaur??" ]},
	],

	"mountains_impassable": [
		{"name": "Asterius",
		"bbcode": [	"Guh!! The wind is too strong!" ]},
		{"name": "Asterius",
		"bbcode": [	"I have to find a way up the mountains..." ]},
	],

	"merchant1": [
		{"name": "Asterius",
		"bbcode": [	"Hello?" ]},
		{"name": "Merchant",
		"bbcode": [	"Brr... so cold...." ]},
#		{"name": "Asterius",
#		"bbcode": [	"This looks like the right place..." ]},
	],
	"merchant2": [
		{"name": "Merchant",
		"bbcode": [	"...! Oh, heavens!        ",
					"Finally, a nice and warm fire!" ]},
	],
	"merchant3": [
		{"name": "Merchant",
		"bbcode": [	"Oh!",
					"Are you the gentleman who helped me stoke the fire?" ]},
		{"name": "Asterius",
		"bbcode": [	"I guess?" ]},
		{"name": "Merchant",
		"bbcode": [	"Sice a kindhearted fellow!\nHere, in return for your help!" ]},
#		{"name": "Merchant",
#		"bbcode": [	"Oh!",
#					"A customer! Hello there!",
#					"Pardon me, I didn't see you there." ]},
#		{"name": "Merchant",
#		"bbcode": [	"Would you like to take a look at my wares?" ]},
#		{"name": "Asterius",
#		"bbcode": [	"Sure, why not." ]},
	],
	"merchant4": [
		{"name": "Merchant",
		"bbcode": [	"Hope you've bundled up good!" ]},
	],

	### ALCHEMISTS

	"alchemists1a": [
		{"name": "Pauli",
		"bbcode": [	"Uhhh..." ]},
		{"name": "Pauli",
		"bbcode": [	"Brother, a monster got in again..." ]},
		{"name": "Pauli",
		"bbcode": [	"I told you those hinges weren't sturdy enough!" ]},
	],
	"alchemists1b": [
		{"name": "Wolfgang",
		"bbcode": [	"AAAHHHH!!" ]},
	],
	"alchemists1c": [
		{"name": "Asterius",
		"bbcode": [	"I'm NOT a monster!" ]},
		{"name": "Wolfgang",
		"bbcode": [	"A talking boar??",
					"Fascinating!..." ]},
		{"name": "Asterius",
		"bbcode": [	"I'M NOT A BOAR EITHER!!" ]},
		{"name": "Pauli",
		"bbcode": [	"Then, what are you, if you don't mind me asking?" ]},
		{"name": "Asterius",
		"bbcode": [	"I'm a minotaur from Crete, I'm looking for [color=#FF00FF]Odin[/color]." ]},
		{"name": "Pauli",
		"bbcode": [	"Odin, eh?",
					"What do you want with him?" ]},
		{"name": "Asterius",
		"bbcode": [	"I heard he searches for [color=#FF0000]great warriors[/color]." ]},
		{"name": "Pauli",
		"bbcode": [	"That he does... You're in luck then, mr. Minotaur. The Big Boss is always roaming around these mountains." ]},
		{"name": "Pauli",
		"bbcode": [	"I'd suggest checking out the peaks of Muspelheim to the east. You might find him there." ]},
		{"name": "Wolfgang",
		"bbcode": [	"Yes, but the mountain pass is too",
					"dangerous for mortals. The [color=#FF00FF]never-",
					"ending blizzard[/color] prevents any from",
					"continuing!" ]},
		{"name": "Asterius",
		"bbcode": [	"My name is Asterius!\nAlso, how do I get across it?" ]},
		{"name": "Wolfgang",
		"bbcode": [	"Hmmm... you truly ARE in luck, mister! We happen to be the greatest students of alchemy in the region!" ]},
		{"name": "Pauli",
		"bbcode": [	"Well, Wolfgang here is. I barely can brew the coffee." ]},
		{"name": "Wolfgang",
		"bbcode": [	"If you'd be willing to ahem, test",
					"some of our concoctions, I've got",
					"one for [color=#FF00FF]controlling the weather[/color]!" ]},
		{"name": "Wolfgang",
		"bbcode": [	"However, in return for the favor..." ]},
		{"name": "Asterius",
		"bbcode": [	"Aaand here it starts." ]},
		{"name": "Wolfgang",
		"bbcode": [	"There's a ferocious wolf that lives",
					"in the forest west of our cabin, it",
					"makes gathering ingredients extremely dangerous..." ]},
		{"name": "Wolfgang",
		"bbcode": [	"My brother Pauli almost got killed last time we ventured there." ]},
		{"name": "Pauli",
		"bbcode": [	"Indeed, that beast is no joke- at least for us. A big, tough warrior like you shouldn't have any problem with it, I bet!" ]},
		{"name": "Asterius",
		"bbcode": [	"You bet right. It will be child's play." ]},
		{"name": "Wolfgang",
		"bbcode": [	"Fantastic! Then, as a token of good will, here's something that might help you." ]},
	],
	"alchemists1d": [
		{"name": "Pauli",
		"bbcode": [	"Good luck!" ]},
	],
	"alchemists1e": [
		{"name": "Wolfgang",
		"bbcode": [	"Return to me when you killed the [color=#FF0000]wolf west of here[/color]!" ]},
	],


	"alchemists2a": [
		{"name": "Pauli",
		"bbcode": [	"Did you already kill the wolf? Talk to my brother Wolfgang, then." ]},
	],
	"alchemists2b": [
		{"name": "Asterius",
		"bbcode": [	"The wolf is dead." ]},
		{"name": "Wolfgang",
		"bbcode": [	"That's fantastic!\nThank you so much for the help!" ]},
		{"name": "Wolfgang",
		"bbcode": [	"As promised, in return, here's a [color=#FF00FF]blizzard-dispelling charm[/color]." ]},
	],

	"alchemists2c": [
		{"name": "Asterius",
		"bbcode": [	"Thank you." ]},
		{"name": "Wolfgang",
		"bbcode": [	"By the way, if you want to [color=#FF00FF]increase",
					"your maximum health[/color] more, I'll",
					"I'll give you more tonic in exchange for [color=#FF0000]600 hero points[/color]." ]},
		{"name": "Wolfgang",
		"bbcode": [	"And of course, if you're ever in need of potions, hit us up! We'll give you a permanent 1000-for-999 discount." ]},
		{"name": "Asterius",
		"bbcode": [	".....Thanks." ]},
	],

	"alchemists3a": [
		{"name": "Pauli",
		"bbcode": [	"My brother and I came here looking for peace and tranquility. I like the place, but the forest is dangerous." ]},
	],
	"alchemists3b": [
		{"name": "Wolfgang",
		"bbcode": [	"Did you want to increase your [color=#FF00FF]maximum health[/color] again? Only [color=#FF0000]600 hero points[/color]!" ]},
	],

	### MUSPELHEIM

	"muspelheim_arrive": [
		{"name": "Asterius",
		"bbcode": [	"Odin... I'm coming for you!" ]},
	],
	"giants_boss_1": [
		{"name": "Asterius",
		"bbcode": [	"..." ]},
	],
	"giants_boss_2": [
		{"name": "Asterius",
		"bbcode": [	"Alright...       \nBring it on!!" ]},
	],
	"giants_boss_3": [
		{"name": "Asterius",
		"bbcode": [	"...And stay dead!!      \nHUFF..!!" ]},
	],

	"odin1": [
		{"name": "Asterius",
		"bbcode": [	"??" ]},
		{"name": "Odin",
		"bbcode": [	"Mortal..." ]},
		{"name": "Odin",
		"bbcode": [	"I don't know what business brings you here, but you've done me a great service bringing those creatures\ndown." ]},
		{"name": "Asterius",
		"bbcode": [	"Are you Odin?" ]},
		{"name": "Odin",
		"bbcode": [	"Indeed, I am." ]},
		{"name": "Asterius",
		"bbcode": [	"Perfect! I've been looking all over for you. Do you have any quests?" ]},
		{"name": "Odin",
		"bbcode": [	"Any what?" ]},
		{"name": "Asterius",
		"bbcode": [	"Do you have any mighty trials for heroes? Challenges? Villages to clear? Bosses to defeat? Horses to capture... Anything you need??" ]},
		{"name": "Odin",
		"bbcode": [	"Hmm...           " ]},
		{"name": "Odin",
		"bbcode": [	"I have no need for lackeys. However, if you truly feel inclined to proving your worth to me, I might benefit from your endevours." ]},
		{"name": "Odin",
		"bbcode": [	"Defeat the other [color=#FF00FF]lords of these\nrealms[/color], mortal, and I will gift thee a place at my side in the eternal home of Valhalla." ]},
		{"name": "Asterius",
		"bbcode": [	"Sounds good to me. It'll be easy." ]},
		{"name": "Odin",
		"bbcode": [	"You are very brave, mortal. Pride is measure of great gifts...\nor great [color=#FF0000]foolishness.[/color]" ]},
		{"name": "Asterius",
		"bbcode": [	"HAH! I'll show you my might!" ]},
		{"name": "Odin",
		"bbcode": [	"Good luck on your \"quest\", mortal." ]},
	],

	"flame_grab1": [
		{"name": "Asterius",
		"bbcode": [	"This looks like it could come in handy!" ]},
	],
	"flame_grab2": [
		{"name": "Asterius",
		"bbcode": [	"And now... on to the next BOSS!" ]},
	],

	### VANHEIM

	"vanheim1": [
		{"name": "Guard",
		"bbcode": [	"Halt! Who goes there?" ]},
		{"name": "Asterius",
		"bbcode": [	"Let me pass, human! I'm a HERO on a quest!!" ]},
		{"name": "Asterius",
		"bbcode": [	"I'm here to defeat the [color=#FF0000]boss of this land[/color]!" ]},
		{"name": "Guard",
		"bbcode": [	"What nonsense is this?? Are you after her majesty, [color=#FF00FF]Lady Freyr[/color]?" ]},
		{"name": "Asterius",
		"bbcode": [	"I have no idea who that is, but that sounds like a boss to me!" ]},
		{"name": "Guard",
		"bbcode": [	"You'll have to walk over my corpse!!" ]},
	],
	"vanheim2": [
		{"name": "Asterius",
		"bbcode": [	"Ready or not, here I come!" ]},
	],
	"vanheim3a": [
		{"name": "Freyr",
		"bbcode": [	"Stay where you are, mortal." ]},
		{"name": "Asterius",
		"bbcode": [	"Are you the boss?" ]},
		{"name": "Freyr",
		"bbcode": [	"I am the goddess of Sun and queen of this realm. What do you want with me?" ]},
		{"name": "Asterius",
		"bbcode": [	"I am a hero and I was sent here by Odin to defeat bosses!" ]},
		{"name": "Freyr",
		"bbcode": [	"Oh god, not that old fool again. So you're just another one of his stupid followers?" ]},
		{"name": "Asterius",
		"bbcode": [	"I'm not stupid, nor am I anyone's follower!! I am a HERO!" ]},
		{"name": "Freyr",
		"bbcode": [	"Hmm, is that so...                 \nYou say you are here to defeat \"bosses\", is that so?",
					"I might have a proposal for you, mortal." ]},
		{"name": "Asterius",
		"bbcode": [	"I'm listening..." ]},
		{"name": "Freyr",
		"bbcode": [	"I don't know what deal you have with Odin, but I know he's been coveting my realm for eons." ]},
		{"name": "Freyr",
		"bbcode": [	"If you do a favor for me, I will compensate you kingly.",
					"I will also leave you proof that you have \"defeated\" me to show Odin." ]},
		{"name": "Asterius",
		"bbcode": [	"Hmm... what kind of favor?" ]},
		{"name": "Freyr",
		"bbcode": [	"A powerful being has recently taken residence in my neighboring realm, Alfheim. They have donned themselves with the title of \"Sun deity\" to mock me." ]},
		{"name": "Freyr",
		"bbcode": [	"I want you to go there and defeat them for me." ]},
		{"name": "Asterius",
		"bbcode": [	"Well... I guess this really doesn't change my plans much. You've got a deal, lady." ]},
		{"name": "Freyr",
		"bbcode": [	"Splendid. I want to put my trust in you, mortal, so to aid you in your challenge, I will bestow upon you one of our sacred weapons." ]},

	],
	"vanheim3b": [
		{"name": "Freyr",
		"bbcode": [	"It is a bow made from the hair of the mother goddess, inbued with the magic of the-" ]},
	],
	"vanheim3c": [
		{"name": "Asterius",
		"bbcode": [	"..." ]},
		{"name": "Freyr",
		"bbcode": [	"...as I was saying. I bestow upon you the ancient art of earthen magic." ]},
	],
	"vanheim3d": [
		{"name": "Freyr",
		"bbcode": [	"May it serve you well." ]},
	],
	"vanheim3e": [
		{"name": "Freyr",
		"bbcode": [	"Godspeed!" ]},
	],

	"vanheim4a": [
		{"name": "Asterius",
		"bbcode": [	"I have defeated the wolf- er, goddess." ]},
		{"name": "Freyr",
		"bbcode": [	"Incredible.. You are suprising, mortal. In you lives the soul of a true hero, indeed!" ]},
		{"name": "Freyr",
		"bbcode": [	"As thanks for your bravery, I gift you this. One of my most previous possessions." ]},
	],
	"vanheim4b": [
		{"name": "Freyr",
		"bbcode": [	"Bring this to Odin as proof of my \"defeat\".\nThank you sincerely for your help!" ]},
		{"name": "Asterius",
		"bbcode": [	"It was a pleasure." ]},
		{"name": "Asterius",
		"bbcode": [	"Now... onto the next boss!" ]},
	],

				##	"......................................" 	  <--- what 38 characters look like
}
