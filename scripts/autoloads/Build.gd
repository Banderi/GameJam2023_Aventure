extends Node

onready var BUILD_NUMBER = IO.read("res://buildnumber.txt",true).strip_edges()
const PROTOCOL_V = 1
const PREFIX = "alpha"

func get_version():
	return str(PREFIX," ",BUILD_NUMBER)

func is_newer(compare_to):
	var current = BUILD_NUMBER.split(".")
	compare_to = compare_to.split(".")
	for i in range(3):
		if int(compare_to[i]) < int(current[i]):
			return false
		if int(compare_to[i]) > int(current[i]):
			return true
	return false


const RSA_KEY_PUBLIC = """-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA2tJzeQ2ebcdH7eF7kdVt
f1Hab22VqxRPcp/4u4ESJLvilp1A/HxwjKegq8MLCAiMdrzCWrvVPVVOLm8Ln3vm
t27xiHJN16tAhSnAfAlQCTTWlAiL6okeU4/aFH0TUTe8aImxP4YBrCSUHPWNCWl0
cPVpIZA/HLqm7IObOJ5Jk84zEkjUDFX1dal2tfgScjFEX36lPsnSMxL3mqnHCcYO
o9eOcUnLsXazcxQe3t/pBrhPe1U9Z8wiwzkET2o2kkmtmVMPEsqC7GLWHUKww3EB
Flx0n+keJsI/yWLxBwff9NAD8Thkd7uFFXywUot461aUUtd9sZ1M6PR+Gp4gEyzR
BStw3HCBDzRhlFKMCozrn6kejkD6LVUGAoEwQTZfR6U1TjluxkMrktv0Cm7p08bY
qZy9EZLO7aUDNYp/fZCPU6OQD/DfFiUQ4vTEXrPz2p0pG88jbGXhPIeerBG1gT9q
CiKytyCs2ydp6vK+4oPBzaDZmM3WcLFS144rdqs3qK0GX07BY5hkCvzKs0JLHTdk
bUXL03jhRWFF+gsQNSRPvDJvUaovRNfScjHJKXg22qhhTefNQXriwueWyGiQJN7U
2qrYx8+lDZQ67NqmjL9tdzoISBtQE06QeSuJoR7XbiDWDqviATR2ZYBd4Bku0UJv
VBOEKSGUEv/TEWutiWyJxccCAwEAAQ==
-----END PUBLIC KEY-----"""
onready var crypto = Crypto.new()
var crypto_key = null
func verify_with_rsa(json_body):
	if crypto_key == null:
		crypto_key = CryptoKey.new()
		var err = crypto_key.load_from_string(RSA_KEY_PUBLIC, true)
		if err != OK:
			Log.error(self, err, "Could not load RSA key string!")
			crypto_key = null
			return false
	Log.generic(self,"Checking RSA signature...")
	if !("data" in json_body) || !("signature" in json_body):
		Log.error(self,GlobalScope.Error.ERR_INVALID_DATA,"Missing critical RSA data in response!")
		return false
	if false: # print extended debug logs
		Log.generic(self,str("data: ",json_body.data))
		Log.generic(self,str("signature: ",json_body.signature))
	var data = json_body.data
	var sha256 = str(data).sha256_buffer()
	var signature = Marshalls.base64_to_raw(json_body.signature)
	return crypto.verify(
		HashingContext.HASH_SHA256,
		sha256,
		signature,
		crypto_key)
func parse_server_data(r):
	# check that the response contains valid a body
	if !('body' in r):
		Log.error(self,GlobalScope.Error.ERR_INVALID_DATA,"Missing 'body' structure in response data!")
		return null
	
	# check that the response body is a valid JSON structure containing data and an RSA signature
	var json_body = parse_json(r.body)
	if json_body == null:
		Log.error(self,GlobalScope.Error.ERR_INVALID_DATA,"Broken JSON object!")
		return null
	if !("data" in json_body) || !("signature" in json_body):
		Log.error(self,GlobalScope.Error.ERR_INVALID_DATA,"Missing data and/or signature!")
		return null
	
	# check response code & print errors if present
	if r.response_code != 200:
		if "errors" in json_body:
			Log.error(self,GlobalScope.Error.ERR_CONNECTION_ERROR,json_body.errors)
		else:
			Log.error(self,GlobalScope.Error.ERR_CONNECTION_ERROR,str("Server responded with ",r.response_code))
		return null
	
	# check validity of RSA signature
	var rsa_check = verify_with_rsa(json_body)
	if !rsa_check:
		Log.error(self,GlobalScope.Error.ERR_INVALID_DATA,"RSA checks failed!")
		return null
		
	return json_body.data

const UPSTREAM_CHANNEL = "windows-upstream-alpha"
var latest_upstream = ""
func check_for_updates(channel_name = UPSTREAM_CHANNEL): # fetch latest version published on itch.io
	Cutscenes.delicate(true)
	var data = parse_server_data(yield(XHR.GET(["https://banderi.dynu.net/itchio/asterius", {"poll_only":true,"v":PROTOCOL_V}]), "finished"))
	Cutscenes.delicate(false)
	if data != null:
		if "latest" in data:
			latest_upstream = data.latest
			var vmsg = str(" (current: ",BUILD_NUMBER," --> itch.io: ",latest_upstream,")")
			if latest_upstream != BUILD_NUMBER:
				if is_newer(latest_upstream):
					print("NEWER version on itch.io!",vmsg)
					return 1
				else:
					print("different version on itch.io, but not newer",vmsg)
					return 2
			else:
				print("nothing new.",vmsg)
				return 0
		elif "errors" in data:
			Log.error(self,GlobalScope.Error.ERR_INVALID_DATA,data.errors[0])
			return -1
		else:
			Log.error(self,GlobalScope.Error.ERR_INVALID_DATA,"Missing 'latest' field in response!")
			return -1
	return 0
func get_versions_list(channel_name = UPSTREAM_CHANNEL): # unused...
	Cutscenes.delicate(true)
#	yield(Cutscenes.delicate(true),"completed")
	yield(Cutscenes._empty_yield(),"completed")
#	var data = parse_server_data(yield(XHR.GET(["https://banderi.dynu.net/itchio/asterius", {"all_versions":true,"v":PROTOCOL_V}]), "finished"))
#	if data != null:
#		pass
#	else:
#		pass
	yield(Butler.tests(),"completed")
	return Cutscenes.delicate(false)

var PATCH_DOWNLOAD_XHR: HTTPRequest = null
func download_latest(channel_name = UPSTREAM_CHANNEL): # grab download url from proxy
	Cutscenes.delicate(true)
	var data = parse_server_data(yield(XHR.GET(["https://banderi.dynu.net/itchio/asterius", {"v":PROTOCOL_V}]), "finished"))
	if data != null:
		if "url" in data:
			var download_url = data.url
			var download_path = "user://temp_patch.zip"
			
			# DOWNLOAD!!!!!
			PATCH_DOWNLOAD_XHR = XHR.DOWNLOAD([download_url], download_path)
			yield(PATCH_DOWNLOAD_XHR, "finished")
			if PATCH_DOWNLOAD_XHR.get_downloaded_bytes() != PATCH_DOWNLOAD_XHR.get_body_size():
				return -1
			PATCH_DOWNLOAD_XHR = null
			return 1
		elif "errors" in data:
			Log.error(self,GlobalScope.Error.ERR_INVALID_DATA,data.errors[0])
			return -2
		else:
			Log.error(self,GlobalScope.Error.ERR_INVALID_DATA,"Missing 'url' field in response!")
			return 0
	Cutscenes.delicate(false)
	return -3
func update_pck_and_restart():
	if !OS.has_feature("standalone"):
		return print("Cannot run this from inside the editor!")
	
	# the .PCK contains everything the game needs, except the executable resource files (game exe icon etc.)
	var exe_dir = OS.get_executable_path().get_base_dir()
	if IO.rename(exe_dir+"/Asterius.exe",exe_dir+"/Asterius.exe.bak"):
		if IO.move_file("user://extr/Asterius.exe",exe_dir+"/Asterius.exe",false,true):
			if IO.move_file("user://extr/Asterius.pck",exe_dir+"/Asterius.pck",false,true):
#				clean_up_temp_files() # this causes problems due to weird async issues (??) stuff, soooooo I'll just do it once the game starts
				Game.restart_game()
func clean_up_temp_files():
	IO.delete("user://extr/")
	IO.delete("user://temp_patch.zip")
