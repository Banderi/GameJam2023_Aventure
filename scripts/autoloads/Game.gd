extends Node

var ROOT = null
var WORLD_ROOT = null
var LEVEL = null
var UI = null

###

var use_debug_tools = false
func debug_tools_enabled():
#	if OS.is_debug_build() || use_debug_tools:
	if use_debug_tools:
		return true
	return false

func is_valid_objref(node):
	if node != null && !(!weakref(node).get_ref()):
		return true
	return false
func is_actor_dead(actor):
	if !is_valid_objref(actor):
		return true
	if actor.health_curr <= 0:
		return true
	return false
func is_senigallius():
	if !is_valid_objref(player):
		return false
	return player.name == "Senigallius"

var game_params = {
	"player_name": "",
	"difficulty": -1,

#	"save_slot": -1,
#	"game_time": -1,
	# ...
}
func start_new_game(params):
	clear_game_stats()
	game_params = params.duplicate()
#	yield(Cutscenes.letterbox(1, 1.5),"completed") # slower letterboxing when starting a new game
	load_level("00_labyrinth")

# save / load
var last_save_slot = ""
var saving_disabled = false
var saving_disabled_msg = ""
func load_game(slot):
	if level_loading:
		return
	var file_data = IO.read(str("user://",slot,".sav"), true)
	if file_data == null:
		return Cutscenes.objective_message(str("No savegame found under ",slot,".sav!"))
	clear_game_stats()
	var loaded_game_stats = parse_json(file_data)
	for v in loaded_game_stats:
		if loaded_game_stats[v] is Dictionary:
			for vv in loaded_game_stats[v]:
				GAMESTATS[v][vv] = loaded_game_stats[v][vv]
		else:
			GAMESTATS[v] = loaded_game_stats[v]
	var player_position = str2var(GAMESTATS.player_position)
	yield(load_level(GAMESTATS.level_scene, player_position),"completed")
func save_game(slot):
	if saving_disabled:
		return Cutscenes.objective_message(saving_disabled_msg)
	match game_state:
		"ingame","paused":
			pass
		"cutscene", "cutscene_letterboxing", "dialogue":
			return Cutscenes.objective_message("Can not save during a cutscene!")
		_:
			return Cutscenes.objective_message("Can not save while not ingame!")
	last_save_slot = slot
	if IO.write(str("user://",slot,".sav"),to_json(GAMESTATS)):
		Cutscenes.objective_message(str("Saved game to ",slot,".sav!"))
	ROOT.enable_load_buttons()
func quicksave():
	save_game("quicksave")
func quickload():
	load_game("quicksave")

func lock_saving(disabled, msg = "Saving is disabled via script!"):
	saving_disabled = disabled
	saving_disabled_msg = msg

var level_ready = false
var level_loading = false
func load_level(tscn_name, spawner = null):
	# safeguards
	if level_loading || Cutscenes.is_letterboxing():
		return Cutscenes._empty_yield()
	var level_file_path = str("res://scenes/levels/",tscn_name,".tscn")
	if !IO.file_exists(level_file_path):
		Cutscenes.objective_message("File '%s' does not exist!" % [tscn_name])
		return Cutscenes._empty_yield()
	level_loading = true

	# fade out, then reset ambient sounds and close all menus
	yield(Cutscenes.letterbox(1, 1),"completed")
	
	# load the actual level file into the tree
	var scene = load(str("res://scenes/levels/",tscn_name,".tscn"))
	if scene == null:
		Cutscenes.objective_message("Could not load file '%s'!" % [tscn_name])
		yield(Cutscenes.letterbox(0, 1),"completed")
		level_loading = false
		return Cutscenes._empty_yield()
	var level_node = scene.instance()
	level_ready = false
	if is_valid_objref(LEVEL):
		LEVEL.queue_free()
		yield(LEVEL,"tree_exited")
	LEVEL = null
	GAMESTATS.level_scene = tscn_name
	set_game_state(null) # reset game state -- this will get set to "cutscene" or such during the add_child (_enter_tree) calls
	WORLD_ROOT.add_child(level_node)
	LEVEL = level_node
	level_ready = true
	
	# automatically fade in and set to "ingame" if there is no custom cutscene in the level scripts
	if game_state == null:
		GENERIC_FADE_IN()
	
	# reset ambient sounds and close all menus
	Sounds.play_ambient("")
	Game.ROOT.clear_menu_stack()

	# reset camera & UI
	camera.reset()
	aim_at_node(null,null)
	lock_on(null)
	Cutscenes.camera_goto(null,null,null,null)
	Cutscenes.objective_message("")

	# spawn player at desired location
	game_over = false
	spawn_player_at(spawner)

	# clear previous savegame locks
	lock_saving(false)

	# give default skills
	if is_senigallius():
		GAMESTATS.unlocked_skills = []
		unlock_skill("Javelin",true)
	else:
		unlock_skill("Trample",true)
		unlock_skill("Javelin",false)

	level_loading = false
	
func spawn_player_at(place):
	if !level_ready || !is_valid_objref(player):
		return
	var position = Vector3()
	if place is Vector3:
		position = place
	elif place is String && place != "":
		var node = LEVEL.get_node(place)
		if is_valid_objref(node):
			position = node.get_global_translation() # assume this is CORRECT.
		else:
			return
	else:
		return
	player.set_global_translation(position)
	player._on_spawn()

func GENERIC_FADE_IN():
	# FADE IN
	yield(Cutscenes.letterbox(0, 1),"completed")
	set_game_state("ingame")

func get_level_scene_name():
	return GAMESTATS.level_scene
func get_level_name():
	match get_level_scene_name().to_lower():
		"00_labyrinth","01_library":
			return "Crete"
		"02_mountain","02b_alchemisthut":
			return "Jotunheim"
		"03_highmountains":
			return "Muspelheim"
		"04_vanheim":
			return "Vanheim"
		"05_alfheim":
			return "Alfheim"
		"06_nidavellir":
			return "Nidavellir"
		"07_niflheim":
			return "Niflheim"
		"08_asgard":
			return "Asgard"
	return ""

var game_over = false
func game_win(): # victory!! credits roll n all
	pass
func game_end_demo():
	Cutscenes.objective_message("to be continued......")
	Cutscenes.title_card(str("Final time:\n",get_play_time()))
	quit_to_main_menu()
func game_over(reason): # failure!! :(
	game_over = true
	Cutscenes.title_card(reason)
	yield(Cutscenes.letterbox(1, 1.5),"completed")
	yield(Cutscenes.wait(1),"completed")
	quit_to_main_menu()

# gamestate
var game_state = null
func set_game_state(state):
	game_state = state
func pause_resume():
	pass
func quit_to_main_menu():
	yield(Cutscenes.letterbox(1, 1.5),"completed")
	ROOT.clear_menu_stack()
	ROOT.open_menu("MainMenu")
	Sounds.play_music("tutorial_loop.ogg")
	Sounds.play_ambient("")
	set_game_state(null)
	yield(Cutscenes.wait(1),"completed")
	yield(Cutscenes.letterbox(0, 1.5),"completed")
func quit_game():
	get_tree().quit()
func reload_tree():
	get_tree().reload_current_scene()
	Game.clear_game_stats()
func restart_game():
	if !OS.has_feature("standalone"):
		print("Cannot run this from inside the editor!")
	else:
		var pid = OS.execute(OS.get_executable_path(), [], false)
		if pid <= 0:
			Log.error(null,GlobalScope.Error.ERR_CANT_FORK,"Could not run new instance of game!")
		else:
			Game.quit_game()

###

var gravity = Vector3.DOWN * 0.5

var player = null
var camera = null
var debug_camera = null

# aiming reticle
var TARGET_SPRITE = null
var PICKER_SPRITE = null
var locked_target = null
var aimed_target_pick = null
var aimed_target_lockon = null
var aimed_target_guesstimate = null
func aim_at_node(pick, lockon):
	if !is_valid_objref(pick):
		aimed_target_pick = null
		PICKER_SPRITE.hide()
	else:
		aimed_target_pick = pick
		PICKER_SPRITE.show()
		PICKER_SPRITE.position = Viewports.world3D_to_screen(Game.camera, aimed_target_pick.get_3D_center())
	if !is_valid_objref(lockon):
		aimed_target_lockon = null
	else:
		if Input.is_action_pressed("target_hold"):
			PICKER_SPRITE.show()
			PICKER_SPRITE.position = Viewports.world3D_to_screen(Game.camera, lockon.get_3D_center())
			aimed_target_guesstimate = lockon
		else:
			aimed_target_guesstimate = null # is this necessary?...
		aimed_target_lockon = lockon
func lock_on(node):
	locked_target = node
	if !is_valid_objref(locked_target):
		TARGET_SPRITE.hide()
		locked_target = null
	else:
		TARGET_SPRITE.show()
		TARGET_SPRITE.position = Viewports.world3D_to_screen(Game.camera, locked_target.get_3D_center())
func get_pick_aim_target():
	if is_valid_objref(locked_target):
		return locked_target
	if is_valid_objref(aimed_target_pick):
		return aimed_target_pick
	if is_valid_objref(aimed_target_guesstimate):
		return aimed_target_guesstimate
	return null
func get_lockon_aim_target():
	if is_valid_objref(aimed_target_pick):
		return aimed_target_pick
	if is_valid_objref(aimed_target_lockon):
		return aimed_target_lockon
	return null

# cursor aim & raypicking
var PICKER_AREA = null
var max_distance_from_player = 25.0
func cursor_pick_at(collision_results : Array):
	for i in PICKER_AREA.size():
		if collision_results.size() > i:
			PICKER_AREA[i].pick_at(collision_results[i])
		else:
			PICKER_AREA[i].pick_at(null)
func consolidate_cursor_pick_results():
	if !is_valid_objref(Game.player):
		return
	var pick_winner = [null,0,99999]
	var lockon_winner = [null,0,99999]
	for picker in PICKER_AREA:
		# pick
		if is_valid_objref(picker.pick_selected[0]) && picker.pick_selected[2] < pick_winner[2]:
			if picker.pick_selected[0].get_3D_center().distance_to(Game.player.get_3D_center()) < max_distance_from_player:
				pick_winner = picker.pick_selected.duplicate()
		# lock-on
		if is_valid_objref(picker.lockon_selected[0]) && picker.lockon_selected[2] < lockon_winner[2]:
			if picker.lockon_selected[0].get_3D_center().distance_to(Game.player.get_3D_center()) < max_distance_from_player:
				lockon_winner = picker.lockon_selected.duplicate()
	aim_at_node(pick_winner[0],lockon_winner[0])

# interactions with props
var INTERACTION_POINTER = null
var interactable = null
func update_interactives():
	interactable = null
	INTERACTION_POINTER.hide()
	if !is_valid_objref(player) || game_state != "ingame":
		return
	for n in get_tree().get_nodes_in_group("targetable"):
		var reach = 1.0
		if "reach" in n:
			reach = n.reach
		if n.get_3D_center().distance_to(player.get_3D_center()) < reach:
			if n is Actor3D && n.team != 0:
				continue
			if n == player:
				continue
			if interactable != n:
				INTERACTION_POINTER.get_node("Label").text = str("(",Keybinds.get_action_key_text("interact"),")")
			interactable = n
			var point = n.get_3D_height() if n.has_method("get_3D_height") else n.get_global_translation() + Vector3(0,0.5,0)
			INTERACTION_POINTER.position = Viewports.world3D_to_screen(Game.camera, point)
			INTERACTION_POINTER.show()

var is_using_controller = false
var is_mouse_captured = false

###

# game params / flags
const GAMESTATS_VIRGIN = {
	"game_time": 0,
	"last_save": null,
	"level_scene": null,
	"player_position": null,
	"unlocked_skills": [],
	"world_flags": {
		"senigallius_1": false,
		"senigallius_2": false,
		"senigallius_3_arena": false, # arriving at the arena room
		"senigallius_finale": false, # final cutscene, then: library, then Jotunheim cutscene
		"npc_merchant_met": false,
		"npc_merchant_helped": false,
		"npc_merchant_received_prize": false,
		"npc_alchemists_met": false,
		"npc_alchemists_killed_wolf": false, # received: potion of anti-blizzard
		"npc_alchemists_received_prize": false, # quest complete!
		"npc_alchemists_hp_bought_1": false,
		"npc_alchemists_hp_bought_2": false,
		"npc_alchemists_hp_bought_3": false,
		"npc_alchemists_hp_bought_4": false,
		"mountains_impassable": false,
		"unlocked_high_mountains": false,
		"muspelheim_arrived": false,
		"npc_vanir_guards": false,
		"npc_freyr_met": false,
		"npc_freyr_bowpractice_1": false,
		"npc_freyr_bowpractice_2": false, # this will unlock the Boulder Throw
		"npc_villageelder_met": false,
		"npc_villageelder_killed_boar": false,
		"unlocked_forest_mountains": false,
		"BOSS_AMATERASU": false, # this will do the final Freyr cutscene and unlock both Stomp and Sun Gem
		"cutscene_freyr_prize": false,
		"BOSS_TWIN_GIANTS": false, # this will unlock the passageway to the Flame of the Gods
		"npc_odin1": false,
		"flame_received": false, # this will unlock the passageway to the flame of the gods
		"unlocked_niflheim": false, # this requires the flame to melt the ice spikes
		"npc_sailor_met": false,
#		"npc_sailor_got_spices": false, # found on the floor next to the sailor
#		"npc_sailor_got_key": false, # this requires: ??????
		"npc_sailor_received_spices": false, # this requires: item found in Nidavellir
		"npc_alchemists_got_spices": false, # this unlocks: potions
#		"npc_blacksmith_met": false,
#		"npc_blacksmith_freed": false, # this unlocks: ores
		"BOSS_DWARVEN_CONSTRUCT": false, # this unlocks the Dwarven Cube
		"BOSS_DRAUGR": false, # this unlocks the Flame of the Dead
		"unlocked_asgard": false, # this requires the flame lvl. 2 to melt the magic ice wall
		"BOSS_HEIMDALL": false,
		"END": false,
	},
	"current_level": 1,
	"current_xp": 0,
	"current_hp": 100,
#	"current_skill_meter": 0.0,
	"current_skill_selected": "Javelin",
	"max_hp": 100,
	"kill_count": 0,
	# ...
}
var GAMESTATS = {
	# ...
}
func clear_game_stats():
	GAMESTATS = GAMESTATS_VIRGIN.duplicate(true) # for debugging
func get_flag(flag):
	if flag in GAMESTATS.world_flags:
		return GAMESTATS.world_flags[flag]
	else:
		print("ERROR: ",flag," is not a valid game flag!")
func set_flag(flag, value):
	if flag in GAMESTATS.world_flags:
		GAMESTATS.world_flags[flag] = value
	else:
		print("ERROR: ",flag," is not a valid game flag!")

# pause menu stats
var STATS_LABEL = null
func time_convert(time_in_sec):
	var seconds = fmod(time_in_sec,60)
	var minutes = fmod((time_in_sec/60),60)
	var hours = (time_in_sec/60)/60

	#returns a string with the format "HH:MM:SS"
	return "%02d:%02d:%02d" % [hours, minutes, seconds]
func get_play_time():
	return time_convert(GAMESTATS.game_time)
func update_stats_label():
	STATS_LABEL.text = str("    ",player.name if is_valid_objref(player) else "")
	STATS_LABEL.text += str("\nPlay time: ",get_play_time())
	STATS_LABEL.text += str("\n",get_level_name())
	STATS_LABEL.text += str("\nHealth: ",GAMESTATS.current_hp,"/",GAMESTATS.max_hp)
	STATS_LABEL.text += str("\nHero points: ",GAMESTATS.current_xp)
	STATS_LABEL.text += str("\nEnemies killed: ",GAMESTATS.kill_count)
	STATS_LABEL.text += str("\nSkills: ")
	for skill in GAMESTATS.unlocked_skills:
		if skill in skills:
			STATS_LABEL.text += str("\n   ",skill)
	STATS_LABEL.text += str("\nItems: ")
	if get_flag("npc_alchemists_received_prize") && !get_flag("unlocked_high_mountains"):
		STATS_LABEL.text += str("\n   Anti-blizzard spell")

# xp / money / hp
func player_increase_max_hp_by(amount):
	GAMESTATS.max_hp += amount
	GAMESTATS.current_hp += amount
	Cutscenes.title_card(str("Increased maximum health by ",amount,"!"))
func player_add_xp(xp):
	if xp == -1:
		xp = 1
	Sounds.play_sound("coin.wav",player.get_3D_center(),1,"SFX")
	Cutscenes.spawn_text(player.get_3D_center(),str("+",xp),Color(1,1,0),0.15,0.5,0)
	GAMESTATS.current_xp += xp
	ROOT.xp_shake = 10
	ROOT.xp_aquire_slide_timer = 5
func player_spend_xp(xp, target_skill):
	pass
func player_level_up():
	pass
func player_get_XP():
	return GAMESTATS.current_xp

# skills
var skills = [
	"Trample",
	"Boulder",
	"Stomp",
	"Flame",
	"Flame2",
	"Javelin",
]
func cycle_skill(quietly = false):
	var next_skill_idx = skills.find(GAMESTATS.current_skill_selected)
	while true:
		next_skill_idx += 1
		if next_skill_idx >= skills.size():
			next_skill_idx = 0
		if is_skill_unlocked(skills[next_skill_idx]) || skills[next_skill_idx] == GAMESTATS.current_skill_selected:
			break
	GAMESTATS.current_skill_selected = skills[next_skill_idx]
	if !quietly:
		Sounds.play_sound("lever_click1.wav",null,0.35,"SFX")
func get_selected_skill():
	return GAMESTATS.current_skill_selected
func unlock_skill(skill_name : String, unlock):
	if skill_name in skills:
		if unlock:
			if !is_skill_unlocked(skill_name):
				GAMESTATS.unlocked_skills.push_back(skill_name)
				GAMESTATS.unlocked_skills.sort()
		else:
			if is_skill_unlocked(skill_name):
				GAMESTATS.unlocked_skills.erase(skill_name)
				GAMESTATS.unlocked_skills.sort()
			if GAMESTATS.current_skill_selected == skill_name:
				cycle_skill(true)
func is_skill_unlocked(skill_name : String):
	return skill_name in GAMESTATS.unlocked_skills
func select_skill(skill_name : String):
	if is_skill_unlocked(skill_name):
		GAMESTATS.current_skill_selected = skill_name

# damage
var godmode = false
var onehit = false
var nodamage = false
var noskillcooldown = false
var noattackcooldown = false
func reload_last_save():
	load_game(last_save_slot)
func player_died():
	if !is_senigallius():
		Sounds.play_sound("moo_charge_b.wav",null,0.25,"SFX")
	game_over("You died...")
func player_take_hit(damage):
	if godmode:
		return
	Cutscenes.spawn_fx_under_node(ROOT.get_hp_column_tip(),
		"ColumnRubble", ROOT.get_hp_column_tip().get_global_position(), {"emitting":true})
	ROOT.t_hp_shake = 0.5
	Sounds.play_sound("moo_hit.wav",player.get_3D_center(),3,"SFX")
	GAMESTATS.current_hp = clamp(GAMESTATS.current_hp - damage, 0, GAMESTATS.max_hp)
	if GAMESTATS.current_hp <= 0:
		player_died()
func player_restore_hp(amount):
	Sounds.play_sound("coin.wav",player.get_3D_center(),1,"SFX")
	GAMESTATS.current_hp = clamp(GAMESTATS.current_hp + amount, 0, GAMESTATS.max_hp)
func player_full_hp():
	GAMESTATS.current_hp = GAMESTATS.max_hp
func player_get_HP_maximum():
	return GAMESTATS.max_hp
func player_get_HP():
	return GAMESTATS.current_hp

func player_increase_kill_count():
	GAMESTATS.kill_count += 1

func fast_travel(destination):
	pass

func spawn_actor(actor):
	pass
func spawn_prop(prop, position, velocity = Vector3()):
	var node = load(str("res://scenes/props/",prop,".tscn")).instance()
	LEVEL.add_child(node)
	node.set_global_translation(position)
	node.apply_central_impulse(velocity)

###

signal settings_loaded
var user_settings_loaded = false
func refresh_settings(): # for extra/additional hot-reload needs!
	for action in Settings.custom_keybinds:
		Keybinds.reload_from_settings(action)

	Sounds.set_volume("Master",Settings.volume_master)
	Sounds.set_volume("SFX",Settings.volume_sfx)
	Sounds.set_volume("Music",Settings.volume_music)

###

func _ready():
	self.set_pause_mode(2) # Set pause mode to Process
	set_process(true)

	Settings.load_user_data()
	user_settings_loaded = true
	emit_signal("settings_loaded")
	refresh_settings()
	clear_game_stats() # for debugging
func _process(delta):
	GAMESTATS.game_time += delta
	if game_state == "paused" || game_over:
		get_tree().paused = true
	else:
		get_tree().paused = false
		update_interactives()
	if ROOT.is_menu("PauseMenu"):
		update_stats_label()
	if is_valid_objref(player):
		GAMESTATS.player_position = var2str(player.get_global_translation())
	else:
		GAMESTATS.player_position = var2str(null)
	if game_state == "ingame":
		consolidate_cursor_pick_results()
