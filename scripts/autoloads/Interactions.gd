extends Node

func interact_with(node):
	print("Attempted interaction with: ",node," (",node.name,")")
	if self.has_method(node.name):
		if "aiming_at_target" in node:
			node.aiming_at_target = Game.player
		self.call(node.name, node)

func NPC_Wolfgang(node):
	if !Game.get_flag("npc_alchemists_killed_wolf"):
		yield(Cutscenes.dialogue_start("alchemists1e"),"completed")
	else:
		if !Game.get_flag("npc_alchemists_received_prize"):
			Game.set_game_state("cutscene")
			yield(Cutscenes.dialogue_start("alchemists2b"),"completed")
			Cutscenes.title_card("Received: anti-blizzard spell")
			Game.set_flag("npc_alchemists_received_prize",true)
			yield(Cutscenes.wait(3),"completed")
			yield(Cutscenes.dialogue_start("alchemists2c"),"completed")
			Game.set_game_state("ingame")
		else:
			if !Game.get_flag("npc_alchemists_hp_bought_1"):
				yield(Cutscenes.dialogue_start("alchemists3b"),"completed")

	node.aiming_at_target = null
func NPC_Pauli(node):
	if !Game.get_flag("npc_alchemists_killed_wolf"):
		yield(Cutscenes.dialogue_start("alchemists1d"),"completed")
	else:
		if !Game.get_flag("npc_alchemists_received_prize"):
			yield(Cutscenes.dialogue_start("alchemists2a"),"completed")
		else:
			yield(Cutscenes.dialogue_start("alchemists3a"),"completed")
	node.aiming_at_target = null

func AlchemistHutDoor_Enter(node):
	Game.load_level("02b_alchemisthut")
func AlchemistHutDoor_Exit(node):
	Game.load_level("02_mountain","SpawnerHut")

func NPC_Merchant(node):
	if !Game.get_flag("npc_merchant_helped"):
		yield(Cutscenes.dialogue_start("merchant1"),"completed")
	else:
		if !Game.get_flag("npc_merchant_received_prize"):
			yield(Cutscenes.dialogue_start("merchant2"),"completed") # oh! finally!
			yield(Cutscenes.letterbox(1, 1, true),"completed")
			yield(Cutscenes.wait(1),"completed")
			node.show()
			yield(Cutscenes.letterbox(0, 1, true),"completed")
			yield(Cutscenes.dialogue_start("merchant3"),"completed") # oh hi there
			Game.increase_max_hp_by(50)
			Game.set_flag("npc_merchant_received_prize",true)
		else:
			yield(Cutscenes.dialogue_start("merchant4"),"completed")

func FlameOfTheGods(node):
	if !Game.get_flag("flame_received"):
		Game.set_game_state("cutscene")
		yield(Cutscenes.letterbox(0.5, 1),"completed")
		Cutscenes.camera_goto(Vector3(47,54,-83),Vector3(55.4,56.6,-97),false,0.1)
		yield(Cutscenes.wait(3),"completed")
		yield(Cutscenes.dialogue_start("flame_grab1"),"completed")
		Cutscenes.title_card("Received: Flame of the Gods")
		Game.set_flag("flame_received",true)
		yield(Cutscenes.wait(3),"completed")
		yield(Cutscenes.dialogue_start("flame_grab2"),"completed")
		Game.game_end_demo()
