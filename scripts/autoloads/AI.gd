extends Node

const STATE_IDLE = 0
const STATE_AGGRO = 1
const STATE_ENGAGING = 2
const STATE_SEARCHING = 3

func drop_common_loot(position):
	if randf() < 0.05:
		Game.spawn_prop("Heart", position, (Vector3(randf(),randf(),randf()).normalized() + Vector3(0,1,0)).normalized() * 2)
	if randf() < 0.3:
		Game.spawn_prop("Meat", position, (Vector3(randf(),randf(),randf()).normalized() + Vector3(0,1,0)).normalized() * 2)
	if randf() < 0.05:
		Game.spawn_prop("Meat", position, (Vector3(randf(),randf(),randf()).normalized() + Vector3(0,1,0)).normalized() * 2)
