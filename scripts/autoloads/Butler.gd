extends Node

const BUTLER_PATH = "E:/Softwares/butler-windows-amd64"
const PIPE_PATH = BUTLER_PATH + "/a.txt"
var TCP_PEER : TCPHandler = null
var PORT = null
var SECRET = null
var FILE_PIPE : File = null

var butler_secret_exchanged = false
func bruteforce_butlerd_port_from_tasklist(): # not needed anymore.
	var pid = Shell.run_sync('cmd /r tasklist | find "butler"', null, false)[0].split("Console")[0].split(" ",false)
	if pid.size() < 2:
		return -1
	var netstat = Shell.run_sync('cmd /r netstat -ano | find "'+pid[1]+'"', null, false)
	var highest_port = -1
	for l in netstat[0].split("\n",false):
		var port = l.split("127.0.0.1:")[1].split(" ",false)[0].to_int()
		if port > highest_port:
			highest_port = port
	return highest_port
func start():
	if Shell.get_pid("butler") == -1:
		if IO.file_exists(PIPE_PATH):
			print("Deleting previous pipe file...")
			var dir = Directory.new()
			while dir.remove(PIPE_PATH) != OK:
				pass
	
		print("Spawning butler...")
		Shell.execute("cmd /r " + BUTLER_PATH + "/butler daemon --json --dbpath " + BUTLER_PATH + "/db/butler.db > " + PIPE_PATH, false)
	
	FILE_PIPE = File.new()
	while FILE_PIPE.open(PIPE_PATH, File.READ) != OK:
		pass
	
	while true:
		var file_contents = FILE_PIPE.get_as_text()
		if file_contents != "":
			if "butlerd/listen-notification" in file_contents:
				
				var s = file_contents.split("\n",false)
				var last_message = parse_json(s[s.size()-1])
				SECRET = last_message.secret
				PORT = last_message.tcp.address.split(":")[1].to_int()
				
				FILE_PIPE.close()
				FILE_PIPE = null
				break
	
	TCP_PEER = XHR.TCP("127.0.0.1", PORT, null)
	if TCP_PEER == null:
		return false
	print("TCP peer connected successfully!")
		
	var r = yield(request(
		"Meta.Authenticate", {
			"secret": SECRET
		}),"completed")
	if r != null:
		return r.get("ok",false)
	return false
func request(request, params = {}):
	var r = yield(TCP_PEER.push_jsonrpc(request, params),"received_jsonrpc")
	if "error" in r:
		print(r.error.message," (",r.error.code,")")
		return null
	elif "result" in r:
		return r.result
	else:
		return r

# butler data
var butler = {
	"profile": null,
	"installLocation": null,
	"game": null,
	"uploads": null,
}
func tests():
	if TCP_PEER == null:
		if !yield(start(),"completed"):
			return
			
	# enum install location info
	if butler.installLocation == null:
		var locations = yield(request("Install.Locations.List"),"completed").installLocations
		if locations.size() > 0:
			butler.installLocation = locations[0]
		else:
			butler.installLocation = yield(request("Install.Locations.Add",{"path":BUTLER_PATH+"/install"}),"completed").installLocation
	
	# enum profiles & log in
	if butler.profile == null:
		var profiles = yield(request("Profile.List"),"completed").profiles
		if profiles.size() > 0:
			butler.profile = profiles[0]
		else:
			butler.profile = yield(request("Profile.LoginWithAPIKey",{"apiKey":IO.read("user://butler", true)}),"completed").profile
	
	
	# enum Asterius game info from itch.io (requires log in)
	if butler.game == null:
		butler.game = yield(request("Fetch.Game",{"gameId":2109981}),"completed").game
	
	# enum game uploads
	if butler.uploads == null:
		butler.uploads = yield(request("Fetch.GameUploads",{"gameId":2109981, "fresh": true}),"completed").uploads
	
	# install!
	if butler.uploads != null && butler.uploads.size() != 0:
		var r = yield(request("Install.Queue",{"game": butler.game, "installLocationId": butler.installLocation.id}),"completed")
		print(JSON.print(r,"\t"))
	
	
	pass
	
#	var InstallQueue = {
#		"caveId": "",
#		"reason": DownloadReason,
#		"installLocationId": ""
#		"game":	Game,
#		"upload": Upload,
#		"build": Build,
#		"installFolder": "",
#		"stagingFolder": "",
#	}
#	print(JSON.print(yield(request("Install.Queue",InstallQueue),"completed"),"\t"))


#	print(JSON.print(yield(request("Fetch.GameUploads",{"gameId":2109981}),"completed"),"\t"))
#	print(JSON.print(yield(request("Fetch.Game",{"gameId":2109981}),"completed"),"\t"))
#	print(JSON.print(yield(request("Fetch.Commons",{"gameId":2109981}),"completed"),"\t"))
#	print(JSON.print(yield(request("Game.FindUploads",{"game":game}),"completed"),"\t"))


