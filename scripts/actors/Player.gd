#tool
extends Actor3D

export(bool) var use_as_player = true

var skill_cooldown = 0.0
var skill_cooldown_full = 1.0 # used to determine the shader param percentage (range 0..1)
var attack_used = null # used for by animation setter!
func skill_cooldown_set(cooldown):
	if Game.noskillcooldown:
		return
	skill_cooldown_full = cooldown
	skill_cooldown = skill_cooldown_full

# Senigallius
func jab():
	attack_cooldown = 0.25
	attack_used = 0

	if Game.is_valid_objref(aiming_at_target):
		var r = Viewports.raycast(get_3D_center(), aiming_at_target.get_3D_center())
		if r != null && r.size() > 0:
			if r.collider == aiming_at_target:
				var distance = get_3D_center().distance_to(r.position)
				if distance > 1.1:
					Sounds.play_sound("swoosh.wav",get_3D_center(),2,"SFX")
				else:
					if aiming_at_target.has_method("take_hit"):
						aiming_at_target.take_hit(10, get_3D_center().direction_to(aiming_at_target.get_3D_center()), self)
					Sounds.play_sound("sock.wav",get_3D_center(),2,"SFX")
					return
	Sounds.play_sound("swoosh.wav",get_3D_center(),2,"SFX")
func javelin():
	var target = lookat.get_global_translation() + Vector3(0,0.4,0)
	if Game.is_valid_objref(aiming_at_target):
		target = aiming_at_target.get_3D_center()
	elif Game.is_valid_objref(Game.camera) && Game.camera.last_raypick_collisions.size() > 0:
		target = Game.camera.last_raypick_collisions[0].position
	attack_cooldown = 0.25
	attack_used = 1
	Cutscenes.spawn_projectile("Javelin",get_3D_center(), self, target)
	skill_cooldown_set(2)

# Asterius
var punch_count = 0
var punch_reset_t = 0.0
func punch():
	attack_used = 0
	punch_reset_t = 0.5
	match punch_count:
		0,1:
			attack_cooldown = 0.05
		2:
			attack_cooldown = 0.35
	
	# attempt raycast to see if anything got hit!
	var hit_connected = false
	var r = null
	if Game.is_valid_objref(aiming_at_target):
		r = Viewports.raycast(get_3D_center(), aiming_at_target.get_3D_center())
	else:
		r = Viewports.raycast(get_3D_center(), lookat.get_global_translation() + Vector3(0,0.5,0))
	if r != null && r.size() > 0:
		var distance = get_3D_center().distance_to(r.position)
		if distance < 1.7:
			if r.collider.has_method("take_hit") && r.collider.has_method("get_3D_center"):
				match punch_count:
					0,1:
						r.collider.take_hit(10, get_3D_center().direction_to(r.collider.get_3D_center()), self)
					2:
						r.collider.take_hit(20, get_3D_center().direction_to(r.collider.get_3D_center()) * 2.5 + Vector3(0,5,0), self)
			hit_connected = true
	if !hit_connected:
		Sounds.play_sound("swoosh.wav",get_3D_center(),2,"SFX",0.2)
	else:
#		Sounds.play_sound("swoosh.wav",get_3D_center(),1.5,"SFX",0.2)
		Sounds.play_sound("sock.wav",get_3D_center(),2,"SFX",0.4)
		Cutscenes.spawn_fx("HitFX", r.position)
			
	punch_count += 1
	if punch_count > 2:
		punch_count = 0

var trampling_t_stomp = 0
func trample():
	Sounds.play_sound("moo_charge2.wav",null,0.25,"SFX")
	attack_used = 2
	attack_cooldown = 2
	trampling_t_stomp = 0
	skill_cooldown_set(5)
func stomp():
	print("stomp!")
	attack_used = 3
	skill_cooldown_set(5)
func throw_boulder():
	print("boulder!")
	attack_used = 4
	skill_cooldown_set(5)
func flame(v):
	attack_used = 5
	print("flame! (",v,")")
	skill_cooldown_set(5)

var joypad_axis_left = [0,0]
var joypad_axis_right = [0,0]
var speed = 2
func _input(event):
	if Engine.editor_hint || !is_inside_tree() || !use_as_player:
		return
	if Game.game_state != "ingame":
		return

	# basic attack
	if Keybinds.is_joypad_corrected_action_pressed("attack", event):
		if attack_cooldown <= 0:
			if Game.is_senigallius():
				jab()
			else:
				punch()
	
	# skills!
	if Keybinds.is_joypad_corrected_action_pressed("use_skill", event):
		if attack_cooldown <= 0:
			if skill_cooldown <= 0:
				self.call(Game.get_selected_skill().to_lower())
			else:
				Sounds.play_sound("lever_click1.wav",null,0.35,"SFX")
	if Keybinds.is_joypad_corrected_action_pressed("cycle_skills", event):
		Game.cycle_skill()
	
	# interact with props / actors / map / etc.
	if Keybinds.is_joypad_corrected_action_pressed("interact", event):
		var interactable = Game.interactable
		if Game.is_valid_objref(interactable):
			Interactions.interact_with(interactable)

func inputs_continuous(delta):
	movement = Vector3()
	if Game.game_state != "ingame" || navpath_head != null || !use_as_player:
		movement = Vector3()
	else:
		# sprinting
		speed = 4
		if Input.is_action_pressed("sprint"):
			speed = 8

		# directional movement (keyboard)
		if Input.is_action_pressed("move_up"):
			movement += Vector3(0,0,-1)
		if Input.is_action_pressed("move_left"):
			movement += Vector3(-1,0,0)
		if Input.is_action_pressed("move_down"):
			movement += Vector3(0,0,1)
		if Input.is_action_pressed("move_right"):
			movement += Vector3(1,0,0)
		if movement != Vector3():
			movement = movement.normalized()

		# directional movement (controller)
		var axis_0 = Input.get_joy_axis(0,0)
		var axis_1 = Input.get_joy_axis(0,1)
		if abs(axis_0) < Settings.controller_deadzone:
			axis_0 = 0.0
		if abs(axis_1) < Settings.controller_deadzone:
			axis_1 = 0.0
		movement += Vector3(axis_0,0,axis_1)

		# rotate movement
		movement = Viewports.match_camera_phi(movement * speed, Game.camera)

	Debug.prnt("movement speed: ", speed)

func _attack_animation(delta): # virtual function
	match attack_used: # TODO DIFFERENT ANIMATIONS
		0: # punch / javelin jab
			set_animation("attack")
		1: # javelin throw
			set_animation("attack")
		2: # trampling
			var target = get_global_translation() + lookat.translation.normalized() * 2
			set_nav_single_point(target, 6, false, true)
			set_animation("attack")
			trampling_t_stomp -= delta
			if trampling_t_stomp <= 0:
				Sounds.play_sound("stomp2.wav",null,0.5,"SFX")
				Cutscenes.spawn_stomp(get_global_translation(),3,15,self)
				trampling_t_stomp = 0.25
		3: # stomp / groundpound
			set_animation("attack")
		4: # boulder / throw
			set_animation("attack")
		5: # flame
			set_animation("attack")
		_: # any other case
			set_animation("attack")
func _attack_finished():
	navpath_head = null # safeguard
	attack_used = null # not really necessary since it's only used during the _attack_animation() process that is managed externally

func _process(delta):
	if Engine.editor_hint || !is_inside_tree():
		return
	Debug.prnt_newl()
	inputs_continuous(delta)

	skill_cooldown = countdown(skill_cooldown, delta)
	punch_reset_t = countdown(punch_reset_t, delta)
	if punch_reset_t <= 0:
		punch_count = 0
	
	var fell_distance = last_stable_height - get_global_translation().y

	Debug.prnt("attack_used: ", attack_used)
	Debug.prnt("attack_cooldown: ", attack_cooldown)
	Debug.prnt("punch_count: ", punch_count)
	Debug.prnt("punch_reset_t: ", punch_reset_t)
	Debug.prnt("skill_cooldown: ", skill_cooldown)
	Debug.prnt("last_stable_height: ", last_stable_height)
	Debug.prnt("fell distance: ", fell_distance)
	Debug.prnt("navpath: ", navpath_points)
	Debug.prnt("navpath_head: ", navpath_head)
	Debug.prnt("movement_type: ", movement_type)
	if navpath_head != null:
		Debug.Point(navpath_head, Color(0,0,1))
	
	
	
	
	Debug.Vector(get_3D_center(), lookat.get_global_translation() - get_3D_center() + Vector3(0,0.5,0), Color(1,0,1))
	var r = Viewports.raycast(get_3D_center(), lookat.get_global_translation() + Vector3(0,0.5,0))
	if r != null && r.size() > 0:
		Debug.Vector(r.position, r.normal, Color(1,0,1), true)

func _ready():
	if Engine.editor_hint || !is_inside_tree():
		return
	if use_as_player:
		Game.player = self
	else:
		hide()
	remove_from_group("targetable")
