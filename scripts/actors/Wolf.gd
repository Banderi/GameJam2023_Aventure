extends Actor3D

export(bool) var do_hownling = true

var time_till_howl = 10
func _update_AI(delta):
	if ai_state == AI.STATE_IDLE:
		if do_hownling:
			time_till_howl -= delta
			if time_till_howl < 0:
				if randf() <= 0.5:
					Sounds.play_sound("wolf_howl1.ogg",get_3D_center(),15,"SFX")
				else:
					Sounds.play_sound("wolf_howl2.ogg",get_3D_center(),15,"SFX")
				time_till_howl = randf() * 30.0 + 15.0

	if !visible:
		return
	common_enemy_aggro(5.5, 20)
	if ai_state == AI.STATE_ENGAGING && Game.is_valid_objref(aiming_at_target):
		if distance_to(aiming_at_target) <= 1.0:
			stop_moving()
			if attack_cooldown < 1.75:
				set_animation("idle")
			if attack_cooldown <= 0:
				# Attack!!!
				set_animation("attack")
				Sounds.play_sound("wolf_attack3.ogg",get_3D_center(),7,"SFX")
				aiming_at_target.take_hit(20, get_3D_center().direction_to(last_seen_target_position),self)
				attack_cooldown += 2
		else:
			set_nav_single_point(last_seen_target_position, 4, true, true)
func die():
	Sounds.play_sound("ded3.ogg",get_3D_center(),3.5,"SFX")
	.die()
