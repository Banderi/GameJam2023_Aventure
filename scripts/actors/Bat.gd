extends Actor3D

func _update_AI(delta):
	common_enemy_aggro(5.5, 20)
	if ai_state == AI.STATE_ENGAGING && Game.is_valid_objref(aiming_at_target):
		if distance_to(aiming_at_target) <= 1.0:
			stop_moving()
			if attack_cooldown <= 0:
				# Attack!!!
				Sounds.play_sound("bat_squeak.ogg",get_3D_center(),5,"SFX")
				aiming_at_target.take_hit(5, get_3D_center().direction_to(last_seen_target_position),self)
				attack_cooldown += 2
		else:
			set_nav_single_point(last_seen_target_position, 2, false, false)
	else:
		navpath_head = null
func die():
	Sounds.play_sound("bat_kill.ogg",get_3D_center(),3.5,"SFX")
	.die()
