extends Actor3D

func _update_AI(delta):
	common_enemy_aggro(15, 30)
	if ai_state == AI.STATE_ENGAGING && Game.is_valid_objref(aiming_at_target):
		if distance_to(aiming_at_target) <= 30:
			if attack_cooldown <= 0:
				# Attack!!!
				Cutscenes.spawn_projectile("BlueFlame", get_3D_center(), self, last_seen_target_position)
#				Sounds.play_sound("bat_squeak.ogg",get_global_translation(),5,"SFX")
				attack_cooldown += 2
func die():
#	Sounds.play_sound("bat_kill.ogg",get_global_translation(),3.5,"SFX")
	.die()
