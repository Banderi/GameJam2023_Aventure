extends Actor3D

export(bool) var ice = true

var next_attack = "pound"

func grunt():
	var s = randf()
	if s < 0.3:
		Sounds.play_sound("wolf_kill1.ogg",get_3D_center(),7,"SFX")
	elif s < 0.6:
		Sounds.play_sound("wolf_kill2.ogg",get_3D_center(),7,"SFX")
	else:
		Sounds.play_sound("wolf_kill3.ogg",get_3D_center(),7,"SFX")
func ground_pound_fx(attack):
	if enraged:
		Cutscenes.spawn_stomp(get_global_translation(),3.5,-1 if !attack else 12,self,true)
		Sounds.play_sound("stomp.wav",get_global_translation(),15,"SFX")
		Sounds.play_sound("stomp3.wav",get_global_translation(),50,"SFX")
	else:
		if Game.game_state != "ingame":
			Cutscenes.spawn_stomp(get_global_translation(),2.5,-1,self,true)
		else:
			Cutscenes.spawn_stomp(get_global_translation(),2.5,-1 if !attack else 10,self,true)
		Sounds.play_sound("stomp2.wav",get_global_translation(),5,"SFX")

var enraged = false
func enrage():
	stay_still()
	Sounds.play_sound("wolf_attack1.ogg",get_3D_center(),80,"SFX")
	Sounds.play_sound("vine.wav",null,0.3,"SFX")
	enraged = true
	ground_pound_fx(true)
	attack_cooldown = 2

export(NodePath) var twin
func friend_died():
	return !Game.is_valid_objref(get_node(twin))

var proj_spawned = 0
func fireball():
	Cutscenes.spawn_projectile("RedFlame", get_aiming_offset(), self, Game.player, 12)
	proj_spawned += 1
	if enraged:
		attack_cooldown = 0.5
		if proj_spawned >= 6:
			choose_next_attack()
	else:
		attack_cooldown = 0.75
		if proj_spawned >= 3:
			choose_next_attack()
func ice_spike():
	Cutscenes.spawn_projectile("IceSpike", get_aiming_offset(), self, Game.player, 10)
	proj_spawned += 1
	if enraged:
		attack_cooldown = 0.5
		if proj_spawned >= 6:
			choose_next_attack()
	else:
		attack_cooldown = 0.75
		if proj_spawned >= 3:
			choose_next_attack()

var windup = 0
func ground_pound():
	match windup:
		0:
			if get_close_to_player(1.5):
				set_animation("windup")
				stay_still()
				windup = 1
				if enraged:
					attack_cooldown = 0.5
				else:
					attack_cooldown = 1.0
		1:
			set_animation("attack_splat")
			ground_pound_fx(true)
			grunt()

			stay_still()
			choose_next_attack()
			windup = 0

func get_close_to_player(distance):
	if Game.is_valid_objref(Game.player) && distance_to(Game.player) <= distance:
		return true
	else:
		var speed = 2.4
		if enraged:
			speed = 4
		set_nav_single_point(Game.player.get_3D_center(), speed, true, true)
		return false
func stay_still():
	stop_moving()
	aiming_at_target = null

func choose_next_attack():
	if enraged:
		attack_cooldown = 1.0
	else:
		attack_cooldown = 2.0
	var s = randf()
	if s < 0.5:
		next_attack = "pound"
	else:
		next_attack = "magic"

func _update_AI(delta):
	if !is_visible_in_tree() || !Game.is_valid_objref(Game.player) || Game.game_state != "ingame":
		return
	if attack_cooldown <= 0:
		# uh oh...
		if Game.game_params.difficulty >= 2 || health_curr < 0.5 * health || friend_died():
			if !enraged:
				return enrage()

		aiming_at_target = Game.player
		set_animation("idle")
		match next_attack:
			"pound":
				ground_pound()
			"magic":
				set_animation("attack_fire")
				if ice:
					ice_spike()
				else:
					fireball()
			_:
				choose_next_attack()
				set_nav_single_point(Game.player.get_3D_center(), 3, true, true)

var is_dead = false
func die():
	is_dead = true
#	Sounds.play_sound("bat_squeak.ogg",get_3D_center(),10,"SFX")
	grunt() # todo
	.die()

func hit_floor(fell_distance):
	ground_pound_fx(false)
	.hit_floor(fell_distance)
