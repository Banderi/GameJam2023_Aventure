extends Actor3D

func _update_AI(delta):
	common_enemy_aggro(5.5, 20)
	if ai_state == AI.STATE_ENGAGING && Game.is_valid_objref(aiming_at_target):
		if distance_to(aiming_at_target) <= 1.5:
			if attack_cooldown < 1.75:
				set_animation("idle")
			if attack_cooldown <= 0:
				# Attack!!!
				set_animation("attack")
				Sounds.play_sound("wolf_attack3.ogg",get_3D_center(),7,"SFX")
				aiming_at_target.take_hit(20, get_3D_center().direction_to(last_seen_target_position),self)
				attack_cooldown += 2
		else:
			set_nav_single_point(last_seen_target_position, 4, true, true)
func die():
	Sounds.play_sound("bat_squeak.ogg",get_3D_center(),10,"SFX")
	.die()
