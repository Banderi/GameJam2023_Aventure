extends Node

export(String) var setting_label = "test_setting" setget set_name
export(String) var setting_name = "test_setting"
export(bool) var real_time_save = false
var dragging = false

func set_name(label):
	setting_label = label
	refresh_label($Slider.value)
func refresh_label(value):
	$Label.text = str(setting_label,": ",value*100,"%")

func refresh_from_settings():
	$Slider.value = Settings.get(setting_name)
	refresh_label($Slider.value)

func _on_Slider_drag_ended(value_changed):
	dragging = false
	Settings.update(setting_name,$Slider.value, true)
func _on_Slider_drag_started():
	dragging = true

func _on_Slider_value_changed(value):
	refresh_label(value)
	if real_time_save:
		Settings.update(setting_name,$Slider.value, false, false)

func _ready():
	add_to_group("setting_sliders")
	if !Game.user_settings_loaded:
		yield(Game,"settings_loaded")
	refresh_from_settings()


func _on_Control_gui_input(event):
	if Input.is_action_just_pressed("reset_setting_to_default"):
		var default_value = Settings.get_default(setting_name)
		$Slider.value = default_value
		_on_Slider_drag_ended(default_value)
