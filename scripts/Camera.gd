extends Camera

export(bool) var use_for_debugging = false

var camera_distance = 8.0
var camera_offset_neutral = Vector3(0,1,1).normalized()
var camera_lookat = Vector3()

var camera_angles = [0,0]
var last_camera_angles = [0,0]
var orbiting = false
func clamp_camera_angles():
	camera_angles[1] = clamp(camera_angles[1], -0.325, 0.525)
func clamp_camera_zoom():
	camera_distance = clamp(camera_distance, 4, 16.0)
func combined_offset():
	var offset = camera_offset_neutral\
		.rotated(Vector3(1,0,0), camera_angles[1])\
		.rotated(Vector3.UP, camera_angles[0])
	return offset
func phi_offset():
	return camera_offset_neutral.rotated(Vector3.UP, camera_angles[0])
func theta_offset():
	return camera_offset_neutral.rotated(Vector3(1,0,0), camera_angles[1])

func reset():
	camera_angles = [0,0]
	last_camera_angles = [0,0]
	orbiting = false
	camera_distance = 8.0

func _physics_process(delta): # smooth camera MOVEMENT needs to be inside _physics_process()
	if !Game.level_ready || !Game.is_valid_objref(Game.player):
		return

	# camera position
	var position_target = Game.player.get_global_translation() + combined_offset() * camera_distance
	var speed = 10
	if Game.locked_target != null:
		var lockon_direction = Game.player.get_global_translation().direction_to(Game.locked_target.get_global_translation())
		var lockon_offset = (-lockon_direction * (1.75) + Vector3(0,2.25,0)) * camera_distance * 0.5
		position_target = Game.player.get_global_translation() + lockon_offset
	if Cutscenes.camera_position != null:
		position_target = Cutscenes.camera_position # no support for Actor3D here because why would we?
	if Cutscenes.camera_speed != null:
		speed = speed * Cutscenes.camera_speed
	if Cutscenes.camera_speed != -1: # -1 is INSTANT MOVEMENT
		if Cutscenes.camera_flyby:
			position_target = Viewports.smoothslide(get_global_translation(), position_target, speed, delta)
		else:
			position_target = Viewports.clampdamp(get_global_translation(), position_target, speed, delta)
	set_global_translation(position_target)
func _process(delta): # smooth camera LOOKAT / ORBIT needs to be inside _process()
	if !Game.level_ready || !Game.is_valid_objref(Game.player):
		return

	# camera lookat -- this normally targets the player, or the targeted object, but during
	# cutscenes it can be a point in space, an object, or a smooth flyby transition.
	var lookat_target = Game.player.get_3D_center()
	if Game.locked_target != null:
		lookat_target = lookat_target + (Game.locked_target.get_3D_center() - lookat_target) * Settings.camera_lockon_displacement
	var speed = 10
	if Cutscenes.camera_target != null:
		if Cutscenes.camera_target is Vector3:
			lookat_target = Cutscenes.camera_target
		elif Game.is_valid_objref(Cutscenes.camera_target):
			lookat_target = Cutscenes.camera_target.get_3D_center()
	if Cutscenes.camera_speed != null:
		speed = speed * Cutscenes.camera_speed
	if Cutscenes.camera_speed != -1: # -1 is INSTANT MOVEMENT
		if Cutscenes.camera_flyby:
			camera_lookat = Viewports.smoothslide(camera_lookat, lookat_target, speed, delta)
		else:
			camera_lookat = Viewports.clampdamp(camera_lookat, lookat_target, speed, delta)
	else:
		camera_lookat = lookat_target
	Viewports.correct_look_at(self, get_global_translation(), camera_lookat)
	Debug.Point(camera_lookat,Color(1,1,1))

	# orbit (mouse when RMB is held)
	var y_dir = 1.0 if (Settings.camera_invert_y || Game.is_using_controller) else -1.0 # TODO: separate inverse Y setting for controller
	if use_for_debugging == Input.is_action_pressed("ALT"):
		if Input.is_action_pressed("RMB") && mouse_last_right_click != null:
			var mouse_pos = Viewports.mouse_position()
			var mouse_delta = mouse_pos - mouse_last_right_click
			camera_angles = [
				last_camera_angles[0] + float(-mouse_delta.x) * 0.005 * Settings.camera_sensitivity_x,
				last_camera_angles[1] + float(y_dir * mouse_delta.y) * 0.005 * Settings.camera_sensitivity_y
			]
			clamp_camera_angles()

	# controller axis input (right stick)
	var axis_2 = Input.get_joy_axis(0,2)
	var axis_3 = Input.get_joy_axis(0,3)
	if abs(axis_2) < Settings.controller_deadzone:
		axis_2 = 0.0
	if abs(axis_3) < Settings.controller_deadzone:
		axis_3 = 0.0
	if Input.is_action_pressed("zoom_hold"): # zoom with L2
		camera_distance += axis_3 * 0.75
		clamp_camera_zoom()
	else:
		# camera orbit
		camera_angles[0] -= axis_2 * 0.05 * Settings.camera_sensitivity_x
		camera_angles[1] += y_dir * axis_3 * 0.05 * Settings.camera_sensitivity_y
		clamp_camera_angles()
	
	# print debug info
	if !use_for_debugging:
		var pos = Viewports.get_3D_camera(get_tree()).get_global_translation()
		var target = -get_global_transform().basis.z
		Debug.prnt_newl()
		Debug.prntpadded("camera: ",false,[10],[pos, target])
		Debug.prnt("viewport size: ", OS.get_window_size())
		Debug.prnt("mouse_position: ", Viewports.mouse_position())
		for collision in last_raypick_collisions:
			Debug.Vector(collision.position, collision.normal, Color(1,0,0), true)

# ray picking -- these are the BLOBS areas that are then used to perform aiming,
# targeting and and lockon logic in [Game]; gets called inside _input()
var last_raypick_collisions = []
func udpate_raypicking():
	last_raypick_collisions = []
	if Game.game_state != "ingame" || !Game.is_valid_objref(Game.player):
		return
	
	# raycast!
	if Game.is_using_controller:
		var normal = null
		if Game.player.movement.length_squared() == 0:
			normal = Game.player.get_3D_center().direction_to(Game.player.lookat.get_global_translation())
		else:
			normal = (Game.player.movement - Vector3(0,0.5,0)).normalized()
		last_raypick_collisions = Viewports.raycast_continuous(Game.player.get_3D_center(), normal, 3, [Game.player])
	else:
		last_raypick_collisions = Viewports.mousepick(self, 3, [Game.player])
	
	Game.cursor_pick_at(last_raypick_collisions)

var mouse_last_right_click = null
var mouse_skip_frame = false # horrible hack, but so long as it works.. >:/
func _input(event):
	if !Game.level_ready:
		return
	if Game.game_state != "ingame":
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		return Game.aim_at_node(null,null)

	# targeting
	if current && !use_for_debugging:
		udpate_raypicking()

		# lock-on
		var aim_target = Game.get_lockon_aim_target()
		if Keybinds.is_joypad_corrected_action_pressed("lockon",event) && !orbiting:
			if aim_target == null || Game.locked_target == aim_target:
				Game.lock_on(null)
			else:
				Game.lock_on(aim_target)
				print("targeting: ", aim_target)

	# if debug camera is current, move the normal camera by pressing "ALT"
	if use_for_debugging == Input.is_action_pressed("ALT"):
		
		# zoom
		if Input.is_action_just_pressed("zoom_in"):
			camera_distance = camera_distance * 0.5
		if Input.is_action_just_pressed("zoom_out"):
			camera_distance = camera_distance * 2.0
		if Input.is_action_just_pressed("zoom_reset"):
			camera_distance = 8.0
		clamp_camera_zoom()

		# mouse actions
		if Input.is_action_just_pressed("RMB"):
			mouse_last_right_click = Viewports.mouse_position()
			last_camera_angles = camera_angles
		if Input.is_action_just_released("RMB"):
			mouse_last_right_click = null
		
		# orbit (continuous mouse capture)
		if event is InputEventMouseMotion && Game.is_mouse_captured:
			var y_dir = 1.0 if Settings.camera_invert_y else -1.0
			var mouse_delta = event.relative
			if !mouse_skip_frame:
				camera_angles = [
					camera_angles[0] + float(-mouse_delta.x) * 0.005 * Settings.camera_sensitivity_x,
					camera_angles[1] + float(y_dir * mouse_delta.y) * 0.005 * Settings.camera_sensitivity_y
				]
				clamp_camera_angles()
			mouse_skip_frame = false

		# toggle mouse capture
		if Input.is_action_just_pressed("change_camera_mode"):
			Game.is_mouse_captured = !Game.is_mouse_captured
			mouse_skip_frame = true
		if Game.is_mouse_captured:
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
		else:
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

func _ready():
	if use_for_debugging:
		Game.debug_camera = self
	else:
		Game.camera = self
