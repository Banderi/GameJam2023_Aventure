extends Spatial

export(float) var reach = 1.0

func get_3D_center():
	return get_global_translation()
func get_3D_height():
	return get_global_translation()

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("targetable")
