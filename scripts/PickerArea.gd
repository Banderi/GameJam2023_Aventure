extends Spatial

var last_collider = null
func pick_at(collision):
	last_collider = collision
	if collision != null && collision.size() > 0:
		set_global_translation(collision.position)

var pick_candidates = []
var lockon_candidates = []
var pick_selected = [null,0,99999.0]
var lockon_selected = [null,0,99999.0]

func weed_out(candidates, mouse_pick, min_distance):
	var winner = null
	if candidates.size() == 1:
		winner = candidates[0]
	elif candidates.size() > 1:
		var closest = null
		for n in candidates:
			var distance = 0
			if mouse_pick:
				distance = n.get_3D_center().distance_to(get_global_translation())
			else:
				if Settings.camera_lockon_use_center_of_screen:
					distance = Viewports.world3D_to_screen(Game.camera,n.get_3D_center()).distance_to(OS.get_window_size() * 0.5)
#					Cutscenes.spawn_text(winner.get_3D_center(),str(distance_record),Color(1,1,1),1,0,0)
				else:
					distance = Viewports.world3D_to_screen(Game.camera,n.get_3D_center())\
					.distance_to(Viewports.world3D_to_screen(Game.camera,get_global_translation()))
			if min_distance == null || distance < min_distance:
				min_distance = distance
				closest = n
		winner = closest

	# this is used by the game loop's consolidator to decide between the multiple AreaPicker results!
	var distance_from_camera = 0
	if winner != null:
		distance_from_camera = winner.get_3D_center().distance_to(Game.camera.get_global_translation())
	return [winner, min_distance, distance_from_camera]

func _process(delta):
	pick_selected = [null,0,99999.0]
	lockon_selected = [null,0,99999.0]
	if Game.game_state != "ingame" || last_collider == null:
		$CSGSphere.hide()
		return
	pick_candidates = []
	lockon_candidates = []
	for n in get_tree().get_nodes_in_group("targetable"):
		if n == Game.player:
			continue
		if n is Actor3D:
			if !n.is_visible_in_tree():
				continue
		var distance = n.get_3D_center().distance_to(get_global_translation())
		if distance < 1.0:
			pick_candidates.push_back(n)
		lockon_candidates.push_back(n)
	var record = 1.0
	$CSGSphere.scale = Vector3(1,1,1)*record

	pick_selected = weed_out(pick_candidates,true,1.0)
	lockon_selected = weed_out(lockon_candidates,false,99999.0)

	$CSGSphere.show()
