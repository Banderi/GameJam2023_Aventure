extends KinematicBody
class_name Prop

export(int) var health = 100
var health_curr = null
func take_hit(damage, from, perpetrator):
	# graphics
	Cutscenes.spawn_text(get_3D_center(),round(damage),Color(1,0,0))
	impulse = (from + Vector3(0, 10, 0))

	if health != null:
		health_curr -= damage
		if health_curr <= 0:
			die()
#		$HP/Bar.set_amount(health_curr, health)
func die():
	queue_free()
	if Game.get_pick_aim_target() == self:
		Game.lock_on(null)
		Game.aim_at_node(null,null)

func get_3D_center():
	return get_global_translation() + $CollisionShape.translation

export(float) var mass = 1.0
export(float) var friction = 1.0

var impulse = Vector3()
var velocity = Vector3()
var prev_velocity = Vector3()

func get_friction():
	if is_on_floor():
		var r = get_last_slide_collision()
		if r != null && r.collider is Actor3D:
			return 0.7
		return clamp(1.0 - friction, 0.0, 1.0)
	else:
		return 1.0

func hit_floor(prev_velocity):
	take_hit(abs(prev_velocity.y),Vector3.UP, null)
	var r = get_last_slide_collision()
	if r != null:
		var collider = r.collider
		if collider.has_method("take_hit"):
			collider.take_hit(abs(prev_velocity.y), prev_velocity.normalized(), self)
func _physics_process(delta):
	var was_on_floor = is_on_floor()
	prev_velocity = velocity
	velocity = (velocity + Game.gravity * mass) * get_friction() + impulse
	velocity = move_and_slide_with_snap(velocity, Vector3.DOWN * 0.001, Vector3.UP)
	if !was_on_floor && is_on_floor() && prev_velocity.y < -1:
		hit_floor(prev_velocity)

	# reset inpulses
	impulse = Vector3()

func _ready():
	collision_layer += 8
	collision_mask += 8
	health_curr = health
	add_to_group("targetable")
	add_to_group("props")
