extends KinematicBody
class_name Actor3D

onready var actor_sprite = $ActorSprite
onready var lookat = $LookAt

export(bool) var critical_NPC = false
export(bool) var spawn_close_to_ground = true
export(bool) var immune_to_fall_damage = false
export(bool) var immune_to_knockback = false

var label_text = ""
func print_to_label(text):
	if !$Label3D.visible:
		return
	label_text += text
func label_refresh():
	if !$Label3D.visible:
		return
	$Label3D.text = label_text
	label_text = ""

var aiming_at_target = null

func get_3D_center():
	return get_global_translation() + $CollisionShape.translation
func get_3D_height():
	return get_global_translation() + $CollisionShape.translation * 2
func get_aiming_offset():
	return get_3D_height()

var net_lookat_angle = 0
func update_lookat():
	if lookat == null:
		lookat = $LookAt
	if !Engine.editor_hint:
		if Game.player == self:
			aiming_at_target = Game.get_pick_aim_target()
		if Game.is_valid_objref(aiming_at_target):
			lookat.set_global_translation(last_seen_target_position)
		else:
			if movement != Vector3():
				lookat.translation = movement

	var origin = get_global_transform().origin
	var raw_vector_lookat = lookat.translation
	var raw_vector_camera_x = Viewports.get_3D_camera(get_tree()).get_global_transform().basis.x

	var raw_angle_lookat = Viewports.angle_around_axis(raw_vector_lookat, Vector3(0,1,0))
	var raw_angle_camera = Viewports.angle_around_axis(raw_vector_camera_x, Vector3(0,1,0))
	net_lookat_angle = (raw_angle_camera - raw_angle_lookat) / PI
	while net_lookat_angle < 0:
		net_lookat_angle += 2

#	Debug.Line(origin, origin + raw_vector_lookat, Color(1,1,0))
	print_to_label(str("raw lookat: ", raw_angle_lookat, "\nraw camera: ", raw_angle_camera,"\nnet angle: ", net_lookat_angle))

export(String) var current_animation = "walk" setget set_animation
var fallback_animation = null
func set_animation(animation):
	if animation == current_animation:
		return
	angles_list = null
	current_animation = animation

var angles_list = null
export(bool) var use_mirrored_sprites = true
func cache_angles(force_fallback = null):
	# List of angles suffixes found for the current animation;
	# These will be automatically interpreted and interpolated
	# by the engine to set the sprite to the correct animation.
	# Angles are [0 - 2 * PI] starting from the sprite facing EAST
	# and increasing in angle while rotating clockwise.
	# e.g.:
	#	EAST: 		0	PI	or 0°
	#	SOUTH: 		0.5 PI	or 90°
	#	WEST: 		1	PI	or 180°
	#	NORTH:		1.5	PI	or 270°
	if actor_sprite == null:
		actor_sprite = $ActorSprite
	fallback_animation = force_fallback
	angles_list = []
	var list = actor_sprite.frames.get_animation_names()
	for l in list:
		var i = l.find_last("_")
		if i != -1:
			var base = l.substr(0, i)
			if base == current_animation if fallback_animation == null else fallback_animation:
				angles_list.push_back(l.substr(i,-1))
func update_raw_sprite_anim():
	if !Engine.editor_hint && !is_inside_tree():
		return
	update_lookat()
	var closest = ""
	var record = null
	var is_mirrored = false
	if angles_list == null:
		cache_angles()
	if angles_list == []:
		cache_angles("idle")
		if angles_list == []:
			return
	for a in angles_list:
		var f = float(a.replace("_",""))
		var diff = abs(net_lookat_angle - f)
		var diff2 = abs(net_lookat_angle - f - 2)
		if record == null || diff < record - 0.1 || diff2 < record - 0.1:
			closest = a
			record = min(diff,diff2)
			is_mirrored = false
		if use_mirrored_sprites:
			var f_mirrored = 1 - f
			while f_mirrored < 0:
				f_mirrored += 2
			diff = abs(net_lookat_angle - f_mirrored)
			diff2 = abs(net_lookat_angle - f_mirrored - 2)
			if diff < record - 0.1 || diff2 < record - 0.1:
				closest = a
				record = min(diff,diff2)
				is_mirrored = true
	if record != null:
		actor_sprite.flip_h = is_mirrored
		var final = (current_animation if fallback_animation == null else fallback_animation) + closest
		if actor_sprite.animation != final:
			actor_sprite.play(final)
func update_sprite_clipping():
	$Label3D.text = ""
	var camera = null
	if Engine.editor_hint:
		return
		camera = Viewports.get_3D_camera(get_tree())
	else:
		camera = Game.camera
		# TODO: other sprites fight with the player's sprite sometimes.... UGH.
	if camera != null:


		# pre-check vars
		var collision_is_in_view = true

		# get intersections with nearest objects
		Viewports.correct_look_at($CameraCollider, get_global_translation() + Vector3(0,0.1,0), camera.get_global_translation())
		var space_state = get_world().direct_space_state
		var query_params = PhysicsShapeQueryParameters.new()
		query_params.set_shape($CameraCollider/Area/CollisionShape.shape)
		query_params.transform = $CameraCollider/Area.get_global_transform()
		var collisions  = space_state.collide_shape(query_params)
		for point in collisions:

			var from = camera.translation
			var to = point

			# raycast from the center to find the actual point on the closest surface!
			var point_on_surface = Viewports.raycast($CameraCollider.get_global_translation(), point, [self], 1)
			if point_on_surface != null && point_on_surface.size() > 0:
				to = point_on_surface.position


			var color = Color(0,1,1)
			var pick_from_camera = Viewports.raycast(from, to, [self], 1)
			if pick_from_camera != null && pick_from_camera.size() > 0:
				if to.distance_to(pick_from_camera.position) > 0.1:
					collision_is_in_view = false
					color = Color(0.2,0.5,0.5)
#					Debug.Vector(pick_from_camera.position, pick_from_camera.normal, Color(1,0,0), true)

#			Debug.Line(from, to, Color(1,0.5,0))
#			Debug.Line($CameraCollider.get_global_translation(), to, color)
#			Debug.Point(to, color)

		actor_sprite.no_depth_test = collision_is_in_view

	else:
		print("no camera??")
func update_animations(delta):
	if attack_cooldown > 0:
		_attack_animation(delta)
	else:
		if movement != Vector3():
			set_animation("walk")
		else:
			set_animation("idle")

var fade_t = 1.0
var fade_speed = 0
func fade_out(speed):
	fade_t = 1.0
	fade_speed = -float(speed)
func fade_in(speed):
	fade_t = 0.0
	fade_speed = float(speed)
func do_fade(delta):
	if fade_speed != 0:
		fade_t += fade_speed * delta
		actor_sprite.modulate.a = clamp(fade_t,0.0,1.0)
		if fade_t < 0 || fade_t > 1:
			fade_speed = 0

enum TEAMS {
	Ally = 0,
	Enemy = 1,
}
export(TEAMS) var team = 0 # 0: ally - 1: enemy
var ai_state = AI.STATE_IDLE # 0: chilling/lost target - 1: aggroed/alerted - 2: actively engaging
var ai_wait_before_engaging_timeout = 0
func _update_AI(delta): # virtual function
	pass

#### COMMON AI FUNCS
var last_seen_target_position = null
func distance_to(target):
	return get_3D_center().distance_to(target.get_3D_center())
func can_see(target):
	var start = get_3D_center()
	var from = start
	var exclude = []
	for n in get_tree().get_nodes_in_group("actors"):
		exclude.push_back(n)
	exclude.erase(self)
	exclude.erase(target)
	var r = Viewports.raycast(from, target.get_3D_center(), exclude)
	if r != null && r.size() > 0:
		if r.collider == target:
			return true
		if r.collider is StaticBody:
			return false
	else:
		return true
	return false
func aggro(target):
	Sounds.play_sound("gasp.wav",get_3D_center(),1,"SFX")
	print("!! from ",self)
	Cutscenes.spawn_text(get_3D_height(),"!",Color(1,0,0),0.25,2.5,-0.105)
	ai_state = AI.STATE_AGGRO # aggro!
	aiming_at_target = target
	ai_wait_before_engaging_timeout = 1 # default: one second
func deaggro():
	print("? from ",self)
	Cutscenes.spawn_text(get_3D_height(),"?",Color(0.75,0.75,0.75),0.25,1,-0.01)
	ai_state = AI.STATE_IDLE # lost player...
	aiming_at_target = null
	navpath_head = null
func common_enemy_aggro(min_distance, max_distance):
	match ai_state:
		AI.STATE_IDLE, AI.STATE_SEARCHING:
			if Game.is_valid_objref(Game.player) && team != Game.player.team:
				# check if player is in view
				if distance_to(Game.player) <= min_distance && can_see(Game.player):
					aggro(Game.player)
		AI.STATE_AGGRO:
			if ai_wait_before_engaging_timeout <= 0:
				ai_state = AI.STATE_ENGAGING
		AI.STATE_ENGAGING:
			if Game.is_valid_objref(aiming_at_target) && distance_to(aiming_at_target) < max_distance && can_see(aiming_at_target):
				pass # still actively engaging
			elif navpath_head == null:
				deaggro()

# health, damage, HP etc.
export(int) var health = 100 # the Player actor doesn't use this system.
var health_curr = health
var hit_flicker = 0
func take_hit(damage, direction, perpetrator):
	if !visible:
		return

	if Game.nodamage:
		damage = 0
	elif Game.onehit && perpetrator == Game.player:
		damage = 999999

	# graphics
	Cutscenes.spawn_text(get_3D_center(),round(damage),Color(1,0,0))
	if !immune_to_knockback:
		impulse += (direction + Vector3(0, 3, 0))
	hit_flicker = 2

	if Game.player == self:
		return Game.player_take_hit(damage)
		
	# non-player actors:
	if health != null:
		health_curr -= damage
		if health_curr <= 0:
			die()
			if perpetrator == Game.player:
				Game.player_increase_kill_count()
		$HP/Bar.set_amount(health_curr, health)
		t_healthbar = 1

	if perpetrator != null && perpetrator != aiming_at_target:
		aggro(perpetrator)

signal actor_died
func die():
	emit_signal("actor_died")
	if critical_NPC:
		Game.game_over("Disaster struck these lands...")
	if self == Game.player:
		Game.player_died()
	else:
		Game.player_add_xp(health)
	queue_free()
	AI.drop_common_loot(get_3D_center())
	if Game.get_pick_aim_target() == self:
		Game.lock_on(null)
		Game.aim_at_node(null,null)

onready var last_stable_height = get_global_translation().y
var was_on_floor = false
signal hit_ground
func hit_floor(fell_distance):
	emit_signal("hit_ground")
	# fall damage
	var potential_damage = round(pow(fell_distance,2))
	if fell_distance > 5 && !immune_to_fall_damage:
		take_hit(potential_damage,Vector3.DOWN, null)
	# if fell on an object/actor, make them take damage as well
	var r = get_last_slide_collision()
	if r != null:
		var collider = r.collider
		if collider.has_method("take_hit"):
			collider.take_hit(potential_damage, Vector3.DOWN, self)

export(float) var mass = 1.0
var velocity = Vector3()
var movement = Vector3()
var impulse = Vector3()
func get_friction():
	return 0.05 # TODO

# why are we still here
signal navpath_completed
var navpath_points = []
var navpath_head = null
var navpath_speed = 1.0
func advance_navpaths():
	if navpath_points.size() > 0:
		navpath_head = navpath_points.pop_back()
	else:
		if navpath_head != null:
			clear_navpath()
			set_animation("idle")
			emit_signal("navpath_completed")
			pop_movement_type()
func movement_from_navpath(delta):
	if navpath_head == null:
		return
	# NOTE: assume for now that this is ONLY called either:
	# - on non-player actors
	# - inside cutscenes
	var distance = navpath_head - get_global_translation()
	movement = distance.normalized() * navpath_speed
	lookat.set_global_translation(get_global_translation() + movement)

	var d = distance.length()
	var m = movement.length()

	# clamp movement to destination
	if movement.length() * delta > distance.length():
		movement = distance / delta
		advance_navpaths()
func point_to_ground(point):
	var r = Viewports.raycast(point + Vector3(0,500,0), point + Vector3(0,-9999,0), [self])
	if r != null && r.size() > 0:
		return r.position
	return point
func set_navpath(dirty_path_node, speed, correct_height, stick_to_ground): # use a 3D node to keep it quick and dirty for now
	print("settin a navpath: ",dirty_path_node)
	clear_navpath()
	navpath_speed = speed
	for p in dirty_path_node.get_children():
		var point = p.get_global_translation()
		if correct_height:
			point = point_to_ground(point)
		navpath_points.push_back(point)
	if navpath_points.size() > 0:
		navpath_head = navpath_points.pop_back()
	if stick_to_ground:
		push_movement_type(0)
	else:
		push_movement_type(1)
func set_nav_single_point(point, speed, correct_height, stick_to_ground):
	clear_navpath()
	navpath_speed = speed
	if correct_height:
		navpath_head = point_to_ground(point)
	else:
		navpath_head = point
	if stick_to_ground:
		push_movement_type(0)
	else:
		push_movement_type(1)
func clear_navpath():
	navpath_points.clear()
	navpath_head = null

# just to suffer
var prev_movement_type = null
var movement_type = null
func push_movement_type(new_type):
	prev_movement_type = movement_type
	movement_type = new_type
func pop_movement_type():
	if prev_movement_type != null:
		movement_type = prev_movement_type
		prev_movement_type = null

# G O D
var last_movement_debug = movement
var last_velocity_debug = velocity
var last_displacement_debug
func integrate_movement_physics(delta):

	# cutscene movement
	if Game.game_state != "ingame":
		if movement_type != 2 && movement_type != 3:
			velocity = Vector3()
			movement_type = 2
	else:
		if mass == 0:
			movement_type = 1
		else:
			movement_type = 0

	# add external impulses, step through the navpaths
	movement += impulse
	if navpath_head != null:
		movement_from_navpath(delta)

	# for debug printing
	last_movement_debug = movement
	last_velocity_debug = velocity

	var last_position = get_global_translation()

	match movement_type:
		0: # normal movement: move_and_slide_with_snap, with gravity
			if is_on_floor():
				velocity.y = 0
				velocity = (velocity + Game.gravity * mass) * get_friction() + movement
				velocity = move_and_slide_with_snap(velocity, Vector3.DOWN if !(impulse.y > 0) else Vector3(), Vector3.UP)
			else:
				movement = Vector3()
				velocity = (velocity + Game.gravity * mass)
				velocity = move_and_slide_with_snap(velocity, Vector3(), Vector3.UP)
		1: # no gravity, no snap -- bee-line
			move_and_slide(velocity + movement, Vector3.UP)
		2: # cutscenes
			move_and_slide(velocity + movement, Vector3.UP)
		3: # cutscenes, but with activated physics
			if is_on_floor():
				velocity.y = 0
				velocity = (velocity + Game.gravity * mass) * get_friction() + movement
				velocity = move_and_slide_with_snap(velocity, Vector3.DOWN if !(impulse.y > 0) else Vector3(), Vector3.UP)
			else:
				movement = Vector3()
				velocity = (velocity + Game.gravity * mass)
				velocity = move_and_slide_with_snap(velocity, Vector3(), Vector3.UP)

	# displacement
	var current_position = get_global_translation()
	last_displacement_debug = current_position - last_position

	# safeguards for unreachable points set by navpaths...
	if last_displacement_debug.length() < 0.001:
		clear_navpath()

# I WANNA DIE
func stop_moving():
	navpath_head = null
	movement = Vector3()
	velocity = Vector3()

func _physics_process(delta):
	if !is_inside_tree():
		return

	update_sprite_clipping() # TODO: rewrite this GARBAGE
	integrate_movement_physics(delta)

	# this needs to be HERE because it needs access to the updated movement vector!
	update_animations(delta)

	# this needs to be AFTER the animation updates!
	impulse = Vector3()
	movement = Vector3()

	# fall damage
	if mass != 0:
		var fell_distance = last_stable_height - get_global_translation().y
		if !was_on_floor && is_on_floor(): # splat!
			hit_floor(fell_distance)
		if !is_on_floor(): # auto-letal height
			if fell_distance > 50:
				die()
	was_on_floor = is_on_floor()
	if was_on_floor:
		last_stable_height = get_global_translation().y

var attack_cooldown = 0
func _attack_animation(delta): # virtual function
	pass
func _attack_finished(): # virtual function
	pass

var _t = 0
var t_healthbar = 0
func countdown(v, delta):
	if v > 0:
		return v - delta
	else:
		return 0
func _process(delta):
	if !is_inside_tree():
		return
	_t += delta
	do_fade(delta)

	if !Engine.editor_hint:
		if rotation.y != 0:
			var lookat_global = $LookAt.get_global_translation()
			rotation.y = 0
			$LookAt.set_global_translation(lookat_global)

	# update attack cooldown timers
	if attack_cooldown > 0:
		if Game.noattackcooldown:
			attack_cooldown = 0
		else:
			attack_cooldown -= delta
		if attack_cooldown <= 0:
			_attack_finished()
			attack_cooldown = 0

	# sprite flickering when hit
	if hit_flicker > 0:
		hit_flicker -= delta
		actor_sprite.visible = !actor_sprite.visible
	else:
		actor_sprite.visible = true

	# update the raw sprites
	update_raw_sprite_anim()

	# other UI stuff
	$HP.hide()
	if !Engine.editor_hint && Game.game_state == "ingame":
		ai_wait_before_engaging_timeout = countdown(ai_wait_before_engaging_timeout, delta)
		if Game.is_valid_objref(aiming_at_target) && can_see(aiming_at_target):
			last_seen_target_position = aiming_at_target.get_3D_center()
		_update_AI(delta)

		# update healthbar
		if Game.get_pick_aim_target() == self && team != 0:
			$HP.show()
		else:
			if t_healthbar > 0:
				$HP.show()
				t_healthbar -= delta
			else:
				$HP.hide()
				t_healthbar = 0
		if $HP.visible:
			$HP.rect_position = Viewports.world3D_to_screen(Viewports.get_3D_camera(get_tree()), get_3D_height())

	# debug helpers
	$LookAt.visible = Debug.display_mode
#	$Label3D.visible = Debug.display_mode
	$Label3D.visible = false

	print_to_label(str("\nmovement_type: ",movement_type))
	print_to_label(str("\nai_state: ",ai_state))
	print_to_label(str("\ai_wait_before_engaging_timeout: ",ai_wait_before_engaging_timeout))
	print_to_label(str("\naiming_at_target: ",aiming_at_target))
	print_to_label(str("\nnavpath_head: ",navpath_head))

	label_refresh()

	if Game.player == self:
		Debug.prnt("movement: ",last_movement_debug)
		Debug.prnt("velocity: ",last_velocity_debug)
		Debug.prnt("displacement: ",last_displacement_debug)

func _ready():
	if Engine.editor_hint || !is_inside_tree():
		return
	add_to_group("targetable")
	add_to_group("actors")
	add_to_group("sprites")
	if self != Game.player:
		health_curr = health

	_on_spawn()

func _on_spawn(): # this must be called again if the level loader sets a spawning place!
	if spawn_close_to_ground && mass != 0:
		move_and_collide(Vector3(0,-99999,0))
