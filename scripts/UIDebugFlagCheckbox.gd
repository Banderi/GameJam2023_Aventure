extends Control

var flag_name = "" setget set_flag_name
func set_flag_name(flag):
	flag_name = flag
	$CheckBox/Label.text = flag_name

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if flag_name != "":
		var checked = Game.get_flag(flag_name)
		if checked == null:
			checked = false
		$CheckBox.pressed = checked

func _on_CheckBox_toggled(button_pressed):
	if flag_name != "":
		Game.set_flag(flag_name, button_pressed)
