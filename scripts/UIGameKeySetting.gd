tool
extends Panel

export(String) var key_name = "Jump" setget set_keyname
var action_name = "jump"

func set_keyname(n : String):
	key_name = n
	action_name = n.to_lower().replace(" ", "_")
	$KeyName.text = key_name
	refresh_keybinds()
func refresh_keybinds():
	var mappings = Keybinds.get_mappings(action_name, null, !Engine.editor_hint)
	var event_1 = null
	var event_2 = null
	if mappings == null:
		$Bind1.text = "??"
		$Bind2.text = "??"
	else:
		for m in mappings:
			if m.type == "key" && event_1 == null:
				event_1 = Keybinds.construct_event(m)
			if m.type == "joypad" && event_2 == null:
				event_2 = Keybinds.construct_event(m)
		$Bind1.text = Keybinds.get_event_text(event_1)
		$Bind2.text = Keybinds.get_event_text(event_2)

###

func reset_keybind():
	Keybinds.destroy_settings(action_name)
	refresh_keybinds()
func clear_keybind(prev_bind):
	pass

###

signal keybind_changed
func _input(event):
	if listening_for_keypress != null:
		if Input.is_action_just_pressed("ui_cancel"):
			refresh_keybinds()
			stop_listening()

		if (event is InputEventKey && !(event is InputEventMouseButton) && event.pressed && listening_for_keypress == 0) ||\
		   (event is InputEventJoypadButton && event.pressed && listening_for_keypress == 1):
			Keybinds.event_change(action_name, event)
			refresh_keybinds()
			stop_listening()
			emit_signal("keybind_changed")
		get_tree().set_input_as_handled()

	if event is InputEventMouseButton:
		return
	$Particles2D.emitting = Input.is_action_pressed(action_name)
func _ready():
	refresh_keybinds()

var listening_for_keypress = null
func _on_Bind_pressed(n):
#	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	match n:
		0:
			$Bind1.text = "..."
			get_parent().get_parent().get_parent().get_node("KeypressText").show()
		1:
			$Bind2.text = "..."
			get_parent().get_parent().get_parent().get_node("KeypressText2").show()
	listening_for_keypress = n
func stop_listening():
#	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	listening_for_keypress = null
	$Bind1.pressed = false
	$Bind2.pressed = false
	get_parent().get_parent().get_parent().get_node("KeypressText").hide()
	get_parent().get_parent().get_parent().get_node("KeypressText2").hide()

func _on_ButtonReset_pressed():
	reset_keybind()
