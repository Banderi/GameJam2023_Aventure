extends Node

func _ready():
	get_viewport().connect("gui_focus_changed", self, "_on_focus_changed")
	$UI/AboutMenu/Label.bbcode_text = $UI/AboutMenu/Label.bbcode_text.replace("<VERSION>", Build.get_version())
	$UI/MainMenu/Version.text = Build.get_version()
#	Build.check_for_updates()
#	Build.test_load_pck()

	if OS.is_debug_build():
		Game.use_debug_tools = true
	else:
		Build.clean_up_temp_files() # previous temp download files
	
	Game.ROOT = self
	Game.WORLD_ROOT = $World3D
	Game.UI = $UI
	Game.STATS_LABEL = $UI/PauseMenu/Stats
	Game.TARGET_SPRITE = $World3D/TargetArrow
	Game.PICKER_SPRITE = $World3D/PickerArrow
	Game.PICKER_AREA = [
		$World3D/PickerArea,
		$World3D/PickerArea2,
		$World3D/PickerArea3,
	]
	Game.INTERACTION_POINTER = $World3D/Interact
	Cutscenes.DIALOGUE_BOX = $UI/DialogueBox
	Cutscenes.DIALOGUE_BBCODE = $UI/DialogueBox/TextCrop
	Cutscenes.DIALOGUE_BUTTON = $UI/DialogueBox/BtnDialogueNext
	Cutscenes.DIALOGUE_BUBBLE = $UI/DialogueBox/SpeechBubble
	Cutscenes.DIALOGUE_TIMER = $UI/DialogueBox/Timer
	Cutscenes.LETTERBOX = $UI/Letterbox
	Cutscenes.PORTRAITS = $UI/CharacterPortraits/Control
	Cutscenes.TITLE_CARD = $UI/TitleCards
	Cutscenes.OBJECTIVE_LABEL = $UI/Objective
	Cutscenes.DELICATE = $DelicateBackdrop
	Sounds.MUSIC_PLAYER1 = $MusicPlayer1
	Sounds.MUSIC_PLAYER2 = $MusicPlayer2
	Sounds.AMBIENT = $Ambient
	Debug.init_nodes($CanvasLayer/DEBUG)
	Debug.fps_label = $CanvasLayer/FPS
	Debug.display_mode = 0
	Viewports.space3D = $World3D.get_world().direct_space_state
	$UI/Ingame.self_modulate.a = 0.0

	refresh_level_list_for_debug_buttons()

	# temp check: first save slot file
	if !IO.file_exists("user://quicksave.sav"):
		$UI/MainMenu/VBoxContainer/BtnLoadGame.disabled = true
		$UI/PauseMenu/VBoxContainer/BtnLoadGame.disabled = true

	# add keybinds to setting menu
	var keybinds_list = $UI/SettingsMenu/TabContainer/Keybinds/ScrollContainer/VBoxContainer
	for n in keybinds_list.get_children():
		n.queue_free()
	for k in active_action_keybinds:
		var n = load("res://scenes/UIGameKeySetting.tscn").instance()
		n.key_name = k
		keybinds_list.add_child(n)
		n.connect("keybind_changed",self,"refresh_keybinds_quickguide")
	refresh_keybinds_quickguide()

	# add flags to debug menu
	var flags_list = $UI/DebugMenus/DebugFlags
	for n in flags_list.get_children():
		n.queue_free()
	for f in Game.GAMESTATS_VIRGIN.world_flags:
		var n = load("res://scenes/UIDebugFlagCheckbox.tscn").instance()
		n.flag_name = f
		flags_list.add_child(n)

	# add skills to debug menu
	var skills_list = $UI/DebugMenus/DebugSkillsUnlocked
	for n in skills_list.get_children():
		n.queue_free()
	for s in Game.skills:
		var n = load("res://scenes/UIDebugSkillCheckbox.tscn").instance()
		n.skill_name = s
		skills_list.add_child(n)

	# update settings UI nodes
	$UI/SettingsMenu/TabContainer/Game/VBoxContainer/Control4/CheckBoxLockonCenter.pressed = Settings.camera_lockon_use_center_of_screen
	$UI/SettingsMenu/TabContainer/Game/VBoxContainer/Control7/CheckBoxCameraInvertY.pressed = Settings.camera_invert_y

	# clean up UI states
	Cutscenes.dialogue_end()
	Cutscenes.portrait(null)
	Cutscenes.objective_message("")
	clear_menu_stack()
	$UI/TitleCards.show()

	# properly start Main Menu
	open_menu("MainMenu")
	Sounds.play_music("tutorial_loop.ogg", 0.75)
#	$UI/MainMenu/VBoxContainer/BtnNewGame.grab_focus()

#	Sounds.play_ambient("stomp.wav",0.7)


#	Game.set_flag("senigallius_finale",true)
#	Game.set_flag("npc_alchemists_received_prize",true)
#	Game.load_02_mountain("SpawnerStart")
#	Game.load_03_highmountains("SpawnerStart")

#	Game.set_flag("senigallius_1",true)
#	Game.load_00_labyrinth()

#	Game.start_new_game({})
#
#	Game.load_level("07_nidavellir", false)
#	Game.load_level("02_mountain", false)
#	Sounds.play_music("level2_1.ogg")
#	Game.set_game_state("ingame")

#	Game.load_level("01_library", false)
#	Cutscenes.cutscene_library()

#	$UI/TintedBackdrop.show()
#	Game.set_game_state("paused")
#	open_menu("PauseMenu")
#	_on_BtnSettings_pressed()

#	Game.load_level("00_labyrinth",false)
#	Game.set_game_state("ingame")
#	yield(Cutscenes.letterbox(1, 0.5),"completed")
#	yield(Cutscenes.letterbox(0.5, 1, false),"completed")

#	Game.load_level("01_library", false)
#	Cutscenes.cutscene_library()

#	Game.set_game_state("cutscene")
#	$UI/TintedBackdrop.hide()

	# check for updates on start
	if !OS.is_debug_build():
		_on_BtnUpdates_pressed(false)

func _input(event):
	# update the controller usage state
	if event is InputEventKey || event is InputEventMouseMotion || event is InputEventMouseButton:
		Game.is_using_controller = false
	elif event is InputEventJoypadButton:
		Game.is_using_controller = true
	elif event is InputEventJoypadMotion:
		if abs(Input.get_joy_axis(0,0)) >= Settings.controller_deadzone:
			Game.is_using_controller = true
		if abs(Input.get_joy_axis(0,1)) >= Settings.controller_deadzone:
			Game.is_using_controller = true
		if abs(Input.get_joy_axis(0,2)) >= Settings.controller_deadzone:
			Game.is_using_controller = true
		if abs(Input.get_joy_axis(0,3)) >= Settings.controller_deadzone:
			Game.is_using_controller = true
	if Game.is_using_controller:
		Cursor.hide()
	else:
		if Game.is_mouse_captured && Game.game_state == "ingame":
			Cursor.hide()
		else:
			Cursor.show()
	
	# menu handling & pausing -- Gotta be careful here... this is some nasty spaghetti.
	# ...this is because the pause_menu and ui_cancel overlap in annoying ways due to having to
	# differentiate between the Escape key behavior and the START controller button.
	if Game.game_state == "ingame" || Game.game_state == "paused":
		if Game.game_state == "ingame" && Keybinds.is_joypad_corrected_action_pressed("pause_menu", event): # pause the game with START or ESCAPE
			open_menu("PauseMenu")
			$UI/TintedBackdrop.show()
			Game.set_game_state("paused")
		else:
			var menu_was_closed = false
			if Keybinds.is_joypad_corrected_action_pressed("pause_menu", event) && is_menu("PauseMenu"): # close the pause menu with START or ESCAPE
				menu_was_closed = close_menu()
			elif Keybinds.is_joypad_corrected_action_pressed("ui_cancel", event): # generic case: close the top menu
				print("ui_cancel")
				if !is_menu("MainMenu"):
					menu_was_closed = close_menu()
			if menu_was_closed && is_menu(null):
				Game.set_game_state("ingame")
		
	# debug shortcuts
	if Input.is_action_just_pressed("debug_restart"):
		Game.reload_tree()
	if Input.is_action_just_pressed("debug_quit"):
		Game.quit_game()
	
	# quick save/load
	if Input.is_action_just_pressed("quicksave"):
		Game.save_game("quicksave")
	if Input.is_action_just_pressed("quickload"):
		Game.load_game("quicksave")

	# why is this better to go here?...
	if Game.game_state == "ingame" && Input.is_action_just_pressed("TAB"):
		refresh_keybinds_quickguide()

var t = 0
func _process(delta):
	t += delta

	update_music_effects(delta)

	Debug.prnt_newl()
	Debug.prnt("is_using_controller: ", Game.is_using_controller)

	var p = 0
	for picker in Game.PICKER_AREA:
#		Debug.prnt(str("picker (",picker.name,"): ",picker.last_collider != null," ",picker.pick_selected," : ",picker.lockon_selected))
		if picker.last_collider != null && picker.last_collider.size() != 0:
			p += 1
	Debug.prnt("pickers: ", str(p, "/", Game.PICKER_AREA.size()))
	Debug.prnt_newl()
	
	Debug.prnt("locked_target: ", Game.locked_target)
	Debug.prnt("aimed_target_pick: ", Game.aimed_target_pick)
	Debug.prnt("aimed_target_lockon: ", Game.aimed_target_lockon)

	Debug.prnt("menu_state: ", menu_state)
	Debug.prnt("menu_stack: ", MENU_STACK)
	Debug.prnt("game_state: ", Game.game_state)

	# ingame HUD and graphics
	if Game.game_state == "ingame" && Game.is_valid_objref(Game.player):
		$UI/Ingame.show()

		# HUD: health bar
		var health = Game.player_get_HP()
		if health == Game.player_get_HP_maximum():
			$UI/Ingame/PlayerHUD/Column/Right.self_modulate.a = 1
		else:
			$UI/Ingame/PlayerHUD/Column/Right.self_modulate.a = 0
		$UI/Ingame/PlayerHUD/Column.rect_size.x = 40 + health
		Debug.prnt("health: ", health)

		# HUD: xp / hero points
		var xp = Game.player_get_XP()
		$UI/Ingame/PlayerHUD/XPBox/XP/Label.text = str("x",xp)
		xp_label_shake(delta)

		# player avatars
		if Game.is_senigallius():
			$UI/Ingame/PlayerHUD/Avatar/Asterius1.hide()
			$UI/Ingame/PlayerHUD/Avatar/Asterius2.hide()
			$UI/Ingame/PlayerHUD/Avatar/Asterius3.hide()
			if t_hp_shake > 0:
				$UI/Ingame/PlayerHUD/Avatar/Senigallius1.hide()
				$UI/Ingame/PlayerHUD/Avatar/Senigallius2.show()
			else:
				$UI/Ingame/PlayerHUD/Avatar/Senigallius1.show()
				$UI/Ingame/PlayerHUD/Avatar/Senigallius2.hide()
		else:
			$UI/Ingame/PlayerHUD/Avatar/Senigallius1.hide()
			$UI/Ingame/PlayerHUD/Avatar/Senigallius2.hide()
			if t_hp_shake > 0:
				$UI/Ingame/PlayerHUD/Avatar/Asterius1.hide()
				$UI/Ingame/PlayerHUD/Avatar/Asterius3.hide()
				$UI/Ingame/PlayerHUD/Avatar/Asterius2.show()
			else:
				$UI/Ingame/PlayerHUD/Avatar/Asterius2.hide()
				if Game.player_get_HP() > 40:
					$UI/Ingame/PlayerHUD/Avatar/Asterius1.show()
					$UI/Ingame/PlayerHUD/Avatar/Asterius3.hide()
				else:
					$UI/Ingame/PlayerHUD/Avatar/Asterius1.hide()
					$UI/Ingame/PlayerHUD/Avatar/Asterius3.show()
		hp_shake(delta)

		# skills
		for n in $UI/Ingame/PlayerHUD/SkillBox.get_children():
			n.hide()
		var curr_skill = str(Game.get_selected_skill())
		var skill_sprite = $UI/Ingame/PlayerHUD/SkillBox.get_node(curr_skill)
		if Game.is_valid_objref(skill_sprite):
			skill_sprite.show()
			skill_sprite.material.set_shader_param("cooldown",clamp(Game.player.skill_cooldown / Game.player.skill_cooldown_full, 0, 1))

		# targeting sprites
		if Game.is_valid_objref(Game.locked_target):
			$World3D/TargetArrow.position = Viewports.world3D_to_screen(Game.camera, Game.locked_target.get_global_translation())
			$World3D/TargetArrow.scale = Vector2(1,1) * (0.9 + 0.1 * sin(t*2))
			$World3D/TargetArrow.rotate(delta*0.6)
	else:
		$UI/Ingame.hide()

	# quick stats slider
	if Game.game_state == "ingame" && Input.is_action_pressed("TAB"):
		quickstats_slide = lerp(quickstats_slide, 1, 0.25)
	else:
		quickstats_slide = lerp(quickstats_slide, 0, 0.125)
	if xp_aquire_slide_timer > 0:
		xp_aquire_slide = lerp(xp_aquire_slide, 1, 0.95)
		xp_aquire_slide_timer -= delta
	else:
		xp_aquire_slide = lerp(xp_aquire_slide, 0, 0.05)
	slide_in_graphics($UI/Ingame/GameControls/Label, 0, 1.0 - quickstats_slide)
	slide_in_graphics($UI/Ingame/PlayerHUD/XPBox, 0, min(1.0, (quickstats_slide + xp_aquire_slide)) - 1.0)


	# patch downloading dialogs & progress bar
#	if $PatchDialog.visible || $PatchDownloading.visible || $UI/MainMenu/VBoxContainer/BtnUpdates.disabled:
#		$DelicateBackdrop.show()
#	else:
#		$DelicateBackdrop.hide()
	if Build.PATCH_DOWNLOAD_XHR != null:
		$PatchDownloading/Bar.show()
		$PatchDownloading.get_ok().disabled = true
		var downloaded = Build.PATCH_DOWNLOAD_XHR.get_downloaded_bytes()
		var total = Build.PATCH_DOWNLOAD_XHR.get_body_size()
		var coeff = float(downloaded) / float(total)
		if total > 0:
			$PatchDownloading/Bar.set_amount(downloaded, total)
			$PatchDownloading/Bar/Label2.text = str(downloaded,"/",total," bytes")
		else:
			$PatchDownloading/Bar.set_amount(0, 100)
			$PatchDownloading/Bar/Label2.text = str("Starting download...")

	# debug
	if !$DelicateBackdrop.visible && Game.debug_tools_enabled() && (Game.game_state == null || Game.game_state == "paused" || is_menu("MainMenu") || is_menu("PausedMenu")):
		$UI/DebugMenus.show()
	else:
		$UI/DebugMenus.hide()

func enable_load_buttons():
	$UI/MainMenu/VBoxContainer/BtnLoadGame.disabled = false
	$UI/PauseMenu/VBoxContainer/BtnLoadGame.disabled = false

#########

# music / audio
var music_fitler_coeff = 1.0
func update_music_effects(delta):
	if Game.game_state == "paused":
		music_fitler_coeff = Viewports.clampdamp(music_fitler_coeff,0,12,delta)
	else:
		music_fitler_coeff = Viewports.clampdamp(music_fitler_coeff,1,0.5,delta)
	Sounds.change_filter("Music",0,"cutoff_hz",20500 - (1.0 - music_fitler_coeff) * 20000.0)
	Sounds.change_filter("Ambient",0,"cutoff_hz",20500 - (1.0 - music_fitler_coeff) * 20000.0)
func _on_MusicPlayer1_finished():
	$MusicPlayer1.play()
func _on_MusicPlayer2_finished():
	$MusicPlayer2.play()
func _on_Ambient_finished():
	$Ambient.play()

# keybinds (for the quick controls panel)
const active_action_keybinds = [
		"Move up",
		"Move left",
		"Move down",
		"Move right",
		"Sprint",
#		"Jump",
		"Attack",
#		"Block",
		"Use skill",
		"Cycle skills",
		"Interact",
		"Use item",
#		"Quick heal",
		"Lockon",
		"Target hold",
		"Zoom in",
		"Zoom out",
		"Change camera mode",
#		"Center camera",
#		"Inventory",
#		"Skilltree",
#		"Map",
		"Quicksave",
		"Quickload",
	]
func refresh_keybinds_quickguide():
	$UI/Ingame/GameControls/Label.text = ""
	for k in active_action_keybinds:
		var action_name = k.to_lower().replace(" ", "_")
		$UI/Ingame/GameControls/Label.text += str("\n",k,": ",Keybinds.get_action_key_text(action_name))
	print("keybinds refreshed!")

# graphics slides, shakes etc.
var t_hp_shake = 0
var xp_shake = 0
var quickstats_slide = 0
var xp_aquire_slide_timer = 0
var xp_aquire_slide = 0
func slide_in_graphics(ui, axis, coeff):
	match axis:
		0:
			ui.rect_position.x = coeff * ui.rect_size.x
		1:
			ui.rect_position.y = coeff * ui.rect_size.y
func hp_shake(delta):
	if t_hp_shake > 0:
		t_hp_shake -= delta
	else:
		t_hp_shake = 0
	$UI/Ingame/PlayerHUD.rect_position.y = 60 + 5.0 * sin(50 * t_hp_shake)
func xp_label_shake(delta):
	if xp_shake > 0:
		xp_shake -= delta * 30
	else:
		xp_shake = 0
#	$UI/Ingame/PlayerHUD/XPBox/XP.rect_rotation = 0.35 * sin(5 * t) * xp_shake
	$UI/Ingame/PlayerHUD/XPBox/XP.rect_position.y = -xp_shake
func get_hp_column_tip(): # for access from other nodes...
	return $UI/Ingame/PlayerHUD/Column/Right

# menus
onready var Menus = { # format: [root_node, default_focus_node]
	"MainMenu": [$UI/MainMenu,$UI/MainMenu/VBoxContainer/BtnNewGame],
	"PauseMenu": [$UI/PauseMenu,$UI/PauseMenu/VBoxContainer/BtnSaveGame],
	"NewGame": [null,null],
	"SaveLoad": [$UI/SaveLoadMenu,null],
	"Settings": [$UI/SettingsMenu,$UI/SettingsMenu/TabContainer/Game/VBoxContainer/Control/Slider],
	"About": [$UI/AboutMenu,null],
}
var MENU_STACK = []
var menu_state = null
func is_menu(menu): # check if current topmost (open) menu is the given one
	if menu == null && menu_state == null:
		return true
	if menu_state == null || menu_state.name != menu:
		return false
	return true
func open_menu(menu):
	# release focus and push current menus onto stack
	if menu_state != null:
		if menu_state.last_btn != null:
			menu_state.last_btn.release_focus()
		MENU_STACK.push_back(menu_state)
	
	# set new menu
	var menu_params = Menus[menu]
	menu_state = {
		"name": menu, # menu name
		"ui": menu_params[0],
		"last_btn": menu_params[1] if menu_params[1] != null else menu_params[0], # default button
	}
	
	# update UI
	menu_state.ui.show()
	menu_state.last_btn.grab_focus()
func close_menu():
	# close current menu
	if menu_state != null:
		if menu_state.last_btn != null:
			menu_state.last_btn.release_focus()
		menu_state.ui.hide()
	
		# remove tinted backdrop from pause menu
		if is_menu("PauseMenu"):
			$UI/TintedBackdrop.hide()
	
		# retrieve previous state from stack
		menu_state = MENU_STACK.pop_back()
		
		# update UI
		if menu_state != null:
			menu_state.ui.show()
			menu_state.last_btn.grab_focus()
		return true
	return false
func clear_menu_stack(): # closes ALL menus for scene loading (including "MainMenu")
	print("closing all menus...")
	while menu_state != null:
		close_menu()
	$UI/TintedBackdrop.hide()
	for m in Menus:
		var node = Menus[m][0]
		if node != null:
			node.hide()
func _on_ButtonBack_pressed(): # generic UI control: closes the last open menu
	close_menu()
func _on_focus_changed(control): # this signal is hooked on startup to the scene tree!
	menu_state.last_btn = control

onready var DEBUG_LEVEL_BTN_SCN = load("res://scenes/DebugLevelButton.tscn")
func refresh_level_list_for_debug_buttons():
	for n in $UI/DebugMenus/DebugLevels.get_children():
		n.free()
	for r in IO.dir_contents("res://scenes/levels").files:
		var btn = DEBUG_LEVEL_BTN_SCN.instance()
		btn.name = r.trim_suffix(".tscn")
		$UI/DebugMenus/DebugLevels.add_child(btn)

func _on_BtnNewGame_pressed():
	# TODO: new game params (difficulty, etc.) window before starting
	Game.start_new_game({})
func _on_BtnLoadGame_pressed():
#	Game.quickload()
	open_menu("SaveLoad")
func _on_BtnSettings_pressed():
	open_menu("Settings")
func _on_BtnAbout_pressed():
	open_menu("About")
func _on_BtnUpdates_pressed(manual_check = true):
	$UI/MainMenu/VBoxContainer/BtnUpdates.disabled = true
	var r = yield(Build.check_for_updates(), "completed")
	match r:
		-1: # error
			Cutscenes.objective_message("Error fetching upstream version!")
		1: # new version found
			Cutscenes.delicate(true)
			$PatchDialog.window_title = "New version available!"
			$PatchDialog.dialog_text = str(Build.BUILD_NUMBER," --> ",Build.latest_upstream,"\nUpdate to latest version?")
			$PatchDialog.popup_centered()
		2: # different version (older?)
			if manual_check:
				Cutscenes.delicate(true)
				$PatchDialog.window_title = "Older version found!"
				$PatchDialog.dialog_text = str(Build.BUILD_NUMBER," --> ",Build.latest_upstream,"\nForce downgrade?")
				$PatchDialog.popup_centered()
		0: # no new version
			if manual_check:
				Cutscenes.objective_message("Game is already up to date!")
	$UI/MainMenu/VBoxContainer/BtnUpdates.disabled = false
func _on_BtnUpdatesList_pressed(): # unused...
#	return
#	var r = yield(Build.get_versions_list(), "completed")
	var r = yield(Build.check_for_updates(), "completed")
	if t == -1:
		return
	Cutscenes.delicate(true)
	$PatchDialog.dialog_text = str(Build.BUILD_NUMBER," --> ",Build.latest_upstream,"\nUpdate to latest version?")
	$PatchDialog.popup_centered()
	
	pass # Replace with function body.
func _on_BtnQuitGame_pressed():
	Game.quit_game()

var patch_download_success = false
func _on_PatchDialog_confirmed(): # user decided to update game to the new version
	patch_download_success = false
	$PatchDownloading/Bar.set_amount(0, 100)
	$PatchDownloading/Bar/Label2.text = str("Downloading...")
	$PatchDownloading.get_ok().disabled = true
	$PatchDownloading.get_ok().hide()
	Cutscenes.delicate(true)
	$PatchDownloading.popup_centered()
	var r = yield(Build.download_latest(),"completed")
	match r:
		-3: # ???
			$PatchDownloading/Bar/Label2.text = str("FAILURE: unknown errors...??")
		-2: # itch.io responded with "errors"
			$PatchDownloading/Bar/Label2.text = str("itch.io error!")
		-1: # downloaded bytes does not match expected file size
			$PatchDownloading/Bar/Label2.text = str("FAILURE: downloaded size incorrect!")
		0: # missing "url" in response
			$PatchDownloading/Bar/Label2.text = str("FAILURE: update server error!")
		1: # all good!:
			$PatchDownloading/Bar/Label2.text = str("Extracting...")
			yield(get_tree(),"idle_frame")
			r = IO.unzip("user://temp_patch.zip","user://extr/") # this is synchronous, no need to yield
			if r != OK:
				$PatchDownloading/Bar/Label2.text = str("Error extracting package!")
			else:
				$PatchDownloading/Bar/Label2.text = str("Done!")
				patch_download_success = true
	$PatchDownloading.get_ok().disabled = false
	$PatchDownloading.get_ok().show()
func _on_PatchDownloading_confirmed(): # download has finished, user clicked continue
	if patch_download_success:
		Build.update_pck_and_restart()
	patch_download_success = false

func _on_BtnSaveGame_pressed():
#	Game.quicksave()
	open_menu("SaveLoad")
func _on_BtnQuitToMain_pressed():
	Game.quit_to_main_menu()

func _on_BtnDialogueNext_pressed():
	Cutscenes.dialogue_next()

func _on_meta_URLs_clicked(meta):
	print(meta)
	OS.shell_open(meta)

func _on_CheckBoxLockonCenter_toggled(button_pressed):
	Settings.update("camera_lockon_use_center_of_screen",button_pressed)
func _on_CheckBoxCameraInvertY_toggled(button_pressed):
	Settings.update("camera_invert_y",button_pressed)

func _on_CheckBoxDebugTools_toggled(button_pressed):
	Game.use_debug_tools = button_pressed

# extra debug toggles
func _on_CheckBoxGodMode_toggled(button_pressed):
	Game.godmode = button_pressed
func _on_CheckBoxOneHit_toggled(button_pressed):
	Game.onehit = button_pressed
func _on_CheckBoxNoDamage_toggled(button_pressed):
	Game.nodamage = button_pressed
func _on_CheckBoxNoSkillCooldown_toggled(button_pressed):
	Game.noskillcooldown = button_pressed
func _on_CheckBoxNoAttackCooldown_toggled(button_pressed):
	Game.noattackcooldown = button_pressed


