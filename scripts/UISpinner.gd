extends TextureRect

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rect_rotation += delta * 300.0
	
	# clamp to sensible range. too high values will break due to floating point errors
	while rect_rotation > 360.0:
		rect_rotation -= 360.0
