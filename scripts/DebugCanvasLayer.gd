#tool
extends CanvasLayer

func _ready():
	if Engine.editor_hint:
		custom_viewport  = Viewports.get_3D_viewport(get_tree())
