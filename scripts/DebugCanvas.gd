#tool
extends CanvasItem

var point_offset = Vector2(2.5, 0)
func _draw():
	for p in Debug.todraw["points"]:
		if p[0] is Vector3:
			var canvas_transform = get_global_transform_with_canvas()
			p[0] = Viewports.world3D_to_screen(Viewports.get_3D_camera(get_tree()), p[0])
		draw_line(p[0] - point_offset, p[0] + point_offset, p[1], 5)
	for l in Debug.todraw["lines"]:
		if l[0] is Vector3:
			l[0] = Viewports.world3D_to_screen(Viewports.get_3D_camera(get_tree()), l[0])
		if l[1] is Vector3:
			l[1] = Viewports.world3D_to_screen(Viewports.get_3D_camera(get_tree()), l[1])
		draw_line(l[0], l[1], l[2], l[3])
	Debug.geometry_queued = false
	Debug.empty_geometry_render_queue()

func _process(delta):
	if Engine.editor_hint && get_parent().visible && Debug.canvas != self:
		print("Setting canvas to: ", self)
		Debug.init_nodes(self, true)

#	if Debug.geometry_queued: # meh...
	update()
