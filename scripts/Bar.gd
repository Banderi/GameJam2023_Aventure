tool
extends ColorRect
class_name Bar

export(Color) var bar_color = Color(0,1,0,1) setget set_color

func set_color(c):
	bar_color = c
	$Bar/Bar.color = c

func set_amount(amount, maximum):
	var proportion = float(amount) / float(maximum)
	$Bar/Bar.rect_size.x = proportion * ($Bar.rect_size.x - 4)
