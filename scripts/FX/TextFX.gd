tool
extends Label3D

var speed = 1.5
var gravity = -0.25
var velocity = Vector3()
var jumpiness = 5

func randomize_velocity():
	velocity = Vector3(1,0,0)\
		.rotated(Vector3(1,0,0), 0.25 + 0.25 * PI * randf())\
		.rotated(Vector3(0,1,0), 2 * PI * randf()).normalized() * speed

var t = 0
func _process(delta):
	if t == 0:
		randomize_velocity()
		velocity += Vector3(0,jumpiness,0)
	t += delta

	translate(velocity*delta)
	velocity += Vector3.UP * gravity
#	if velocity.y <= -4:
#		velocity = velocity.normalized()

	if t >= 1:
		if Engine.editor_hint:
			translation = Vector3()
			t = 0
		else:
			queue_free()
