#tool
extends Spatial

var base_scale = Vector3(1,1,1)

var radius = 4
var damage = 1
var caster = null
var ignore_friends = false

var bodies_hit = []

var t = 0
func _process(delta):
	t += delta
	var falloff_coeff = 1.0/pow(10.0, t)

	$stomp.scale = base_scale * t * 2
	$stomp.scale.y = base_scale.y * (0.1 + falloff_coeff)

	$stomp/mesh/Cylinder.get_active_material(0).albedo_color.a = 3 * pow(falloff_coeff, 3)
	$Area/CollisionShape.shape.radius = radius

	if damage != -1:
		if $Area.monitoring:
			for body in $Area.get_overlapping_bodies():
				if ignore_friends && Game.is_valid_objref(caster) && caster is Actor3D && body is Actor3D && body.team == caster.team:
					continue
				if body != caster && !(body in bodies_hit) && body.has_method("take_hit"):
					var direction = body.get_3D_center() - get_global_translation()
					body.take_hit(damage, direction + Vector3(0,0.1,0) * damage, caster)
					bodies_hit.push_back(body)
		if t >= 0.5:
			$Area.monitoring = false
	else:
		$Area.monitoring = false

	if Engine.editor_hint:
		if t > 1:
			t = 0
	else:
		if t > 2:
			queue_free()

func _ready():
	base_scale = base_scale * radius * 0.5
