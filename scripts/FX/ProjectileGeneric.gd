extends Sprite3D

export(float) var destroy_timeout = 2.0
export(float) var speed = 1.0
export(String) var spawn_sound = ""
export(String) var hit_sound = ""
export(float) var spawn_sound_volume = 1.0
export(float) var hit_sound_volume = 1.0
export(int) var damage = 15

var target = null
var direction = Vector3()
var alive = true

var callback = null
var callback_params = []

var caster = null

var t = 0
func _process(delta):
	t += delta
	if t > 5:
		print("despawning projectile: ",self)
		queue_free()
	if !alive:
		return

	if direction == Vector3():
		if target != null:
			if target is Vector3:
				direction = (target - get_global_translation()).normalized()
			elif target.has_method("get_3D_center"):
				direction = (target.get_3D_center() - get_global_translation()).normalized()
			else:
				direction = (target.get_global_translation() - get_global_translation()).normalized()
		else:
			direction = Vector3(randf(),randf(),randf()).normalized()

	var step = speed * direction * delta
	Viewports.correct_look_at(self, get_global_translation() + step, get_global_translation() + direction)

func _on_Timer_timeout():
	queue_free()

func _on_Area_body_entered(body):
	if !alive || body == caster:
		return
	if has_node("Particles"):
		$Particles.emitting = false
	if has_node("Particles2"):
		$Particles2.emitting = true
	alive = false
	opacity = 0

	if hit_sound != "":
		Sounds.play_sound(hit_sound, get_global_translation(), hit_sound_volume, "SFX")

	if body is Actor3D:
		body.take_hit(damage, direction, caster)

	# optional callback
	if target != null:
		var distance = 0
		if target is Actor3D:
			distance = get_global_translation().distance_to(target.get_global_translation())
		else:
			distance = get_global_translation().distance_to(target)
		if distance < 0.25:
			if callback != null:
				callback.call_funcv(callback_params)

	# destroy
	if has_node("Timer") && destroy_timeout >= 0:
		$Timer.start(destroy_timeout)
	else:
		queue_free()
